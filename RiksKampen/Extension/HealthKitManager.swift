//
//  HealthKitManager.swift
//  RiksKampen
//
//  Created by Ikotel on 1/3/19.
//  Copyright © 2019 KamsQue. All rights reserved.
//

import UIKit
import HealthKit
import CoreMotion

protocol HealthServiceDelegate{
    func tracingHealthUpdate(steps: String , distance: String ,floorsAscended : String , floorsDescended : String)
}

class HealthKitManager: NSObject {

    
    var delegate: HealthServiceDelegate?
    var steps : String = ""
    var distance : String = ""
    var floorsAscended : String = ""
    var floorsDescended : String = ""
    
    private let activityManager = CMMotionActivityManager()
    private let pedometer = CMPedometer()
    
    static let sharedInstance:HealthKitManager = {
        let instance = HealthKitManager()
        return instance
    }()
    
    override init() {
        super.init()
        
//
        //Create a NSCalendar
        let calendar = Calendar.current
        //create an NSDate that is the time period for yesterday
        let yesterday = calendar.date(byAdding: .day, value: -1, to: Date())
        //Safely unwrap the optional NSDate
        let today : Date? = Date()
        if let today = today {
            //Use the NSDate to get the data from the CMPedometer instance for yesterday till the current moment
            pedometer.queryPedometerData(from: yesterday ?? Date(), to: Date()) { data, error in
                //Unwrap the data and make sure we didn't run into an error
                guard let activityData = data, error == nil else {
                    print("There was an error getting the data: \(error)")
                    return
                }
                
                self.steps = "\(activityData.numberOfSteps)"
                self.distance = "\(activityData.distance)"
                self.floorsAscended = "\(activityData.floorsAscended)"
                self.floorsDescended = "\(activityData.floorsDescended)"
                
                
                self.tracingHealthUpdate()
                //prints out step count
                print("Steps: \(activityData.numberOfSteps)")
                //print out optional distance in of walking and running in meters
                print("Distance \(activityData.distance)")
                //print out optional floors ascended
                print("Floors ascended: \(activityData.floorsAscended)")
                //print out optional floors descended
                print("Floors descended \(activityData.floorsDescended)")
            }
        }
    }
    
    
    
    private func tracingHealthUpdate(){
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingHealthUpdate(steps: self.steps, distance: self.distance, floorsAscended: self.floorsAscended, floorsDescended: floorsDescended)
        
    }
}
