//
//  AppInternalNetwork.swift
//  RiksKampen
//
//  Created by Ikotel on 12/4/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//
import Foundation
import UIKit



enum Storyboard: String {
    
    case launchScreenStory = "LaunchScreen"
    case loginStoryboard = "Login"
    case mainStoryBoard = "MainStoryboard"
    case profileStory = "ProfileStoryBoard"

}

enum From : String {
    
    case activity
    case trainig
    case nutriation
    
}

class AppInternalNetwork: NSObject {
    
    private static let launchScreenStoryboard = UIStoryboard(name: Storyboard.launchScreenStory.rawValue, bundle: nil)
    private static let loginStoryboard = UIStoryboard(name: Storyboard.loginStoryboard.rawValue, bundle: nil)
    private static let mainStoryBoard = UIStoryboard(name: Storyboard.mainStoryBoard.rawValue, bundle: nil)
    
    static var passwordChangeSuccessMessage = ""
    
    static var latitude : String = String()
    static var longitide : String = String()
    static var place     : String = String()
    
    static func getLoginVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    }
    
    static func getSendEmailVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "SendEmailVC") as! SendEmailVC
    }
    
    
    static func getVerfiCodeVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "VerfiCodeVC") as! VerfiCodeVC
    }
    
    static func getResetPasswordVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
    }
    
    static func getSignUpVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
    }
    
    static func getIntroductionVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "IntroductionVC") as! IntroductionVC
    }
    
    static func getAllTrainingPlansVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "AllTrainingPlansVC") as! AllTrainingPlansVC
    }
    
    static func getPlanAlertVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "PlanAlertVC") as! PlanAlertVC
    }
    
 
    
    static func getPTSignUPVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "PTSignUPVC") as! PTSignUPVC
    }
    
    static func getAppTabBarController() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "AppTabBarController") as! AppTabBarController
    }
    
    static func getHomeVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
    }
    
    static func getMapVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "MapVC") as! MapVC
    }

    static func getTypesTableViewController() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "TypesTableViewController") as! TypesTableViewController
    }
    
    static func getLeaderBoardVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "LeaderBoardVC") as! LeaderBoardVC
    }
    
    static func getNutraitionDetailsVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "NutraitionDetailsVC") as! NutraitionDetailsVC
    }
    
    static func getSettingsVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
    }
    
    static func getActivityDetailsVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "ActivityDetailsVC") as! ActivityDetailsVC
    }
    
    static func getDateCalenderVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "DateCalenderVC") as! DateCalenderVC
    }
    
    static func getWebViewVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
    }
    
    static func getWeeklyVideosVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "WeeklyVideosVC") as! WeeklyVideosVC
    }
    
    static func getVideoDetailsVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "VideoDetailsVC") as! VideoDetailsVC
    }
    
    static func getViewController() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
    }
    
    static func getWeeklyActivitiesVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "WeeklyActivitiesVC") as!
         WeeklyActivitiesVC
    }
   
    
}



