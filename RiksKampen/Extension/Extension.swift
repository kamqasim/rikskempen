//
//  Extension.swift
//  RiksKampen
//
//  Created by Ikotel on 12/4/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import DropDown
import RealmSwift
import AlamofireImage
import CoreLocation

extension UITextField {
    
    func modifyClearButton(with image : UIImage) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(_:)), for: .touchUpInside)
        rightView = clearButton
        rightViewMode = .whileEditing
    }
    
    @objc func clear(_ sender : AnyObject) {
        self.text = ""
        sendActions(for: .editingChanged)
    }
}

extension UISearchBar {
    public func setSerchTextcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
    }
}

extension UITextField
{
    enum Direction
    {
        case Left
        case Right
    }
    
    func AddImage(direction:Direction,imageName:String,Frame:CGRect,backgroundColor:UIColor)
    {
        let View = UIView(frame: Frame)
        View.backgroundColor = backgroundColor
        
        let imageView = UIImageView(frame: Frame)
        imageView.image = UIImage(named: imageName)
        
        View.addSubview(imageView)
        
        if Direction.Left == direction
        {
            self.leftViewMode = .always
            self.leftView = View
        }
        else
        {
            self.rightViewMode = .always
            self.rightView = View
        }
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    func base64(format: ImageFormat) -> String? {
        var imageData: Data?
        
        switch format {
        case .png: imageData = UIImagePNGRepresentation(self)
        case .jpeg(let compression): imageData = UIImageJPEGRepresentation(self, compression)
        }
        
        return imageData?.base64EncodedString()
    }
}

extension String {
    func imageFromBase64() -> UIImage? {
        guard let data = Data(base64Encoded: self) else { return nil }
        
        return UIImage(data: data)
    }
}

extension UIViewController{
    
    
   
    func imagePlacement(imagePath: String , imageView : UIImageView ){
        if !imagePath.isEmpty{
            let url = URL(string: imagePath)!
            let placeholderImage = UIImage(named: "default-placeholder")!
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: imageView.frame.size,
                radius: 0.0
            )
            imageView.af_setImage(
                withURL: url,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .flipFromTop(0.2)
            )
        }
    }
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        if tappedImage.tag == 0{
            CameraHandler.shared.showActionSheet(vc: self)
            CameraHandler.shared.imagePickedBlock = { (image) in
                /* get your image here */
                //self.liberary = true
                tappedImage.image = image
                
                //tappedImage = image.base64(format: .jpeg(0.50))!
            }
        }
    }
    
    func getDate(date: String) -> Date{
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        var inputDate = dateFormatter.date(from: date)
        return inputDate!
    }
    
    func creatingCircle(views: [UIView] , imageViews : [UIImageView]){
        
        for (index, view) in views.enumerated() {
            circleView(View: view)
            circleImageView(imageView: imageViews[index])
        }
    }
    
    func creatingCellCircle(views: [UIView] , buttons : [UIButton]){
        
        for (index, view) in views.enumerated() {
            
            cornerRoundView(View: view)
            circleButton(button: buttons[index])
        }
    }
    
    func circleImageView(imageView : UIImageView){
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        imageView.clipsToBounds = true
    }
    
    func cornerRoundView(View : UIView){
        View.backgroundColor = UIColor(hexString: "ffe59b").withAlphaComponent(0.5)
        View.layer.cornerRadius = View.frame.size.height / 2
        View.clipsToBounds = true
    }
    
    func circleView(View : UIView){
        View.layer.cornerRadius = View.frame.size.width / 2
        View.clipsToBounds = true
    }
    
    func circleButton(button : UIButton){
        button.layer.cornerRadius = button.frame.size.width / 2
        button.clipsToBounds = true
    }
    
    func addShadowWithBorder(shadowView: UIView,color:UIColor,opacity:CGFloat,shadowRadius:CGFloat){
        shadowView.layer.shadowOpacity = 0.7
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        shadowView.layer.shadowRadius = shadowRadius
        shadowView.layer.shadowColor = color.cgColor
    }
    
    func showAlertDialog(title: String ,message: String ,completion: @escaping () -> Void )  {
        let alert = UIAlertController(title: title , message: message , preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Thanks", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func popBackAction(){
        
        let Ok =  UIAlertAction(title: "Yes", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
            
        }
        
        let Cancel =  UIAlertAction(title: "No", style: .destructive) { (action) in
            
        }
        
        self.AlertToSave(alertActions: [Ok,Cancel], message: "Are you sure? changes have not been saved.", title: "Alert" )
    }
    
    
    func setscolorfontSizeAttributedString(strings:[String],founts: [UIFont],color: [UIColor]) -> NSAttributedString{
        
        let myString : NSMutableAttributedString = NSMutableAttributedString()
        for i in 0...strings.count - 1 {
            myString.append(attributedString(string: strings[i],color:color[i], font: founts[i]))
        }
        return myString
    }
    
    func attributedString(string:String,color:UIColor,font: UIFont)->NSAttributedString{
        let myAttribute1 = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: color]
        return NSMutableAttributedString(string: string, attributes: myAttribute1)
    }
    
    func setBorderAndCornerRadius(layer: CALayer, width: CGFloat, radius: CGFloat,color : UIColor ) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    func popVc() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func backToRootTappedPop(){
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    func backTappedPop(){
        DispatchQueue.main.async {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func pushToVC(vc: UIViewController) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func popBack<T:UIViewController>(toControllerType : T.Type){
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers{
            viewControllers.forEach({ (currentViewController) in
                if currentViewController.isKind(of: toControllerType){
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                }
            })
        }
    }
    
    func encodeBase64(image: UIImage) -> String{
        let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64
    }
    
    func isValidName(in text: String) -> Bool
    {
        do
        {
            let regex = try NSRegularExpression(pattern: "^[0-9a-zA-Z\\_]{7,18}$", options: .caseInsensitive)
            if regex.matches(in: text, options: [], range: NSMakeRange(0, text.count)).count > 0 {return true}
        }
        catch {}
        return false
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPassword(testStr: String) -> Bool {
        if testStr.count >= 2 {
            return true
        }
        return false
    }
    
    func isValidCode(testStr: String) -> Bool {
        if testStr.count >= 4 {
            return true
        }
        return false
    }
    
    func isValidNameCount(testStr: String) -> Bool {
        print(testStr)
        if testStr.count >= 6 {
            return true
        }
        return false
    }
    
    func isValidContact(testStr : String  ) -> Bool{
        let contactRegex = "(\\+[0-9]+[\\- \\.]*)?" + "(\\([0-9]+\\)[\\- \\.]*)?"
            + "([0-9][0-9\\- \\.]+[0-9])"
        //let contactRegex = "/^(\\+\\d{1,3}[- ]?)?\\d{10}$/"
        
        let contacttest = NSPredicate(format:"SELF MATCHES %@", contactRegex)
        return contacttest.evaluate(with: testStr)
    }
    
    //String values validation
    func checkFieldStringValues(stringValues : [String] , stringErrors : [String]) -> String {
        var index = 0
        if(stringValues.count != 0 && stringErrors.count != 0){
            for values in stringValues{
                
                if (values == ""){
                    return stringErrors[index]
                }
                index = index + 1
            }
        }
        return ""
    }
    
    func setPlaceHolder(textFileds : [UITextField],placeHolder: [String]){
        for i in 0...textFileds.count - 1 {
            
            textFileds[i].placeholder = placeHolder[i]
        }
    }
    // Alert Need Action YOu Want To do And Message
    
    func AlertToSave(alertActions:[UIAlertAction], message : String ,title: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        for action in alertActions{
            alert.addAction(action)
        }
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func addUserModel(user : UserModel , userId : String , userName: String,age : String , height: String , weight : String ,address: String){
        
        var realm = try? Realm()
        
        user.userid = userId
        user.firstName = userName
        user.address = address
        
        try! realm?.write {
            let user1 = OtherUserList()
            user1.otherUserList.append(user)
            realm?.add(user1, update: false)
        }
    }
    
    func logout(){
        
        let vc = AppInternalNetwork.getLoginVC() as! LoginVC
        let appDelegate = UIApplication.shared.delegate
        
        
        let nav = UINavigationController.init(rootViewController: vc)
        nav.navigationBar.isHidden = true
        nav.interactivePopGestureRecognizer?.isEnabled = false
        appDelegate?.window??.rootViewController = nav
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func setUpDropDown(textfield:UITextField,dataSource:[String],dropdown:DropDown){
        var appearance = DropDown.appearance()
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        appearance.contentMode = .center
        dropdown.anchorView = textfield
        dropdown.bottomOffset = CGPoint(x: 0, y: textfield.bounds.height)
        dropdown.dataSource = dataSource
        dropdown.selectionAction = { [weak self](index,item) in
            textfield.text = item.trimmingCharacters(in: .whitespacesAndNewlines)
            dropdown.hide()
        }
    }
    
    func makeGradient(navView: UIView ,statusbarView:CGRect , color1 : UIColor , color2 : UIColor ,color3: UIColor ) -> CAGradientLayer{
        
        let gradient = CAGradientLayer()
        var bounds = navView.bounds
        bounds.size.height += statusbarView.size.height
        gradient.frame = bounds
        gradient.colors = [color1.cgColor,color2.cgColor,color3.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        return gradient
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
}




extension UINavigationController {
    
    func backToViewController(vc: Any) {
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    
}
extension UINavigationController {
    var rootViewController : UIViewController? {
        return viewControllers.first
    }
}

extension UIView {
    
    // In order to create computed properties for extensions, we need a key to
    // store and access the stored property
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
    }
    
    fileprivate typealias Action = (() -> Void)?
    
    // Set our computed property type to a closure
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    //  create the tap gesture recognizer and
    // store the closure the user passed to us in the associated object we declared above
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // Every time the user taps on the UIImageView, this function gets called,
    // which triggers the closure we stored
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
    
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }    
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func lock() {
        if let _ = viewWithTag(10) {
            //View is already locked
        }
        else {
            let lockView = UIView(frame: bounds)
            lockView.backgroundColor = UIColor(white: 0.0, alpha: 0.75)
            lockView.tag = 10
            lockView.alpha = 0.0
            let activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
            activity.hidesWhenStopped = true
            activity.center = lockView.center
            lockView.addSubview(activity)
            activity.startAnimating()
            addSubview(lockView)
            
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 1.0
            })
        }
    }
    
    func unlock() {
        if let lockView = viewWithTag(10) {
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 0.0
            }, completion: { finished in
                lockView.removeFromSuperview()
            })
        }
    }
    
    func fadeOut(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
    func fadeIn(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as? UIView
    }
    
    
}
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue:      CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}

extension UIView {
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}
extension Notification.Name {
    static let didReceiveData = Notification.Name("didReceiveData")
    static let didCompleteTask = Notification.Name("didCompleteTask")
    static let completedLengthyDownload = Notification.Name("completedLengthyDownload")
}
extension Date {
    func monthAsString() -> String {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MMM")
        return df.string(from: self)
    }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}
