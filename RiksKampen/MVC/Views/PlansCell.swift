//
//  PlansCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/16/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import DLRadioButton

class PlansCell: UITableViewCell {

    @IBOutlet weak var prizeLbl : UILabel!
       @IBOutlet weak var ageLimitLbl : UILabel!
       @IBOutlet weak var juniorLbl : UILabel!
       @IBOutlet weak var singleLbl : UILabel!
       @IBOutlet weak var duolLbl : UILabel!
       @IBOutlet weak var backgroundImageView : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
