//
//  OrderCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/7/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

  
    @IBOutlet weak var itemImageView: UIImageView!
    
    
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var itemDescriptionLbl : UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var itemStatusLbl: UILabel!
    @IBOutlet weak var likeDislikeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
