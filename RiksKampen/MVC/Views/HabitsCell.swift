//
//  HabitsCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/7/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class HabitsCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var habitView: UIView!
    @IBOutlet weak var habitNameLbl: UILabel!
    
    @IBOutlet weak var joinPeopleLbl: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
    
}
