//
//  MapPlacesCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/9/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class MapPlacesCell: UICollectionViewCell {
    
    @IBOutlet weak var ImageView : UIImageView!
    @IBOutlet weak var placeName: UILabel!
    
}
