//
//  ProgressCellUpdate.swift
//  RiksKampen
//
//  Created by Ikotel on 12/13/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class ProgressCellUpdate: UITableViewCell {

    @IBOutlet weak var progressView : UIView!
    @IBOutlet weak var progressViewWidth: NSLayoutConstraint!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var positionNumber: UILabel!
    @IBOutlet weak var profileImageContestant: UIImageView!
    
    @IBOutlet weak var caloriesAndStepsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
