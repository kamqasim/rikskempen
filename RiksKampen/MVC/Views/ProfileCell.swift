//
//  ProfileCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/13/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var leftImageView : UIImageView!
    @IBOutlet weak var titleName : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
