//
//  MarkerInfoView.swift
//  RiksKampen
//
//  Created by Ikotel on 12/4/18.
//  Copyright © 2018 KamsQue. All rights reserved.

import UIKit

class MarkerInfoView: UIView {
  
  @IBOutlet weak var placePhoto: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
}
