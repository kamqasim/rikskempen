//
//  VideoCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/17/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var videoNameLbl: UILabel!
}
