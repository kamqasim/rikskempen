//
//  WWCalenderTimeSelctorDelegateHelper.swift
//  Rowtisserie
//
//  Created by Hafiz Muhammad Junaid on 31/01/2018.
//  Copyright © 2018 Appabilities. All rights reserved.
//

import Foundation
import UIKit

class WWCalenderTimeDelegateHelper: NSObject {
    
    var controller : UIViewController!
    var dateTimePicker : WWCalendarTimeSelector!
    var isDatePicker : Bool!
    var selectedDate: Date? = nil
    
    
    typealias didFinishPickingDateOrTime = (_ dateTime:String)->Void
    fileprivate var pickingDateOrTime:didFinishPickingDateOrTime!
    fileprivate let themeColor = UIColor.green
    init(controller : UIViewController) {
        
        super.init()
        self.controller = controller
        
        self.setDateTimePicker()
    }
    fileprivate func setDateTimePicker() {
        
        dateTimePicker = WWCalendarTimeSelector.instantiate()
        self.dateTimePicker.delegate = self
        dateTimePicker.optionCalendarBackgroundColorTodayHighlight = themeColor
        dateTimePicker.optionCalendarBackgroundColorPastDatesHighlight =  themeColor
        dateTimePicker.optionCalendarBackgroundColorFutureDatesHighlight =  themeColor
        dateTimePicker.optionClockBackgroundColorAMPMHighlight =  themeColor
        dateTimePicker.optionClockBackgroundColorHourHighlight =  themeColor
        dateTimePicker.optionClockBackgroundColorHourHighlightNeedle =  themeColor
        dateTimePicker.optionClockBackgroundColorMinuteHighlight =  themeColor
        dateTimePicker.optionClockBackgroundColorMinuteHighlightNeedle =  themeColor
        dateTimePicker.optionButtonFontColorCancel = themeColor
        dateTimePicker.optionButtonFontColorDone = themeColor
        dateTimePicker.optionButtonFontColorCancelHighlight = themeColor
        dateTimePicker.optionButtonFontColorDoneHighlight = themeColor
        dateTimePicker.optionTopPanelBackgroundColor = themeColor
        dateTimePicker.optionSelectorPanelBackgroundColor =  themeColor
        dateTimePicker.optionCalendarFontColorDays = UIColor.black
        dateTimePicker.optionCalendarFontColorToday = UIColor.black
        dateTimePicker.optionCalendarFontColorPastDates = UIColor.lightGray
        dateTimePicker.optionCalendarFontColorFutureDates = UIColor.black
        dateTimePicker.optionCurrentDateRange.setStartDate(Date())
        
    }
    
    func showDatePicker(date:Date, completion:@escaping  didFinishPickingDateOrTime) {
        self.setDateTimePicker()
        
        self.isDatePicker = true
        self.pickingDateOrTime = completion
        
        dateTimePicker.optionCurrentDate = date
        dateTimePicker.optionStyles.showDateMonth(true)
        dateTimePicker.optionStyles.showMonth(false)
        dateTimePicker.optionStyles.showYear(true)
        dateTimePicker.optionStyles.showTime(false)
        self.controller.present(dateTimePicker, animated: true, completion: nil)
        
    }
    
    func showTimePicker(time:Date, completion:@escaping didFinishPickingDateOrTime) {
        self.setDateTimePicker()
        self.isDatePicker = false
        self.pickingDateOrTime = completion
        dateTimePicker.optionCurrentDate = time
        
        dateTimePicker.optionStyles.showDateMonth(false)
        dateTimePicker.optionStyles.showMonth(false)
        dateTimePicker.optionStyles.showYear(false)
        dateTimePicker.optionStyles.showTime(true)

        self.controller.present(dateTimePicker, animated: true, completion: nil)
    }
    
}

//MARK: - WWCalendarTimeSelectorProtocol
extension WWCalenderTimeDelegateHelper : WWCalendarTimeSelectorProtocol {
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        
        self.selectedDate = date
        if isDatePicker {
            let dateString = date.stringFromFormat("YYYY-MM-dd")
            
            self.pickingDateOrTime(dateString)
        }
        else {
            
            let dateString = date.stringFromFormat("HH:mm:ss")
            self.pickingDateOrTime(dateString)
        }
        
    }
    
    func WWCalendarTimeSelectorShouldSelectDate(_ selector: WWCalendarTimeSelector, date: Date) -> Bool {
        
        let currentDate = Date()
//         || date.addingTimeInterval(86400.0) >= currentDate.updateDay(dayValue: 9)
        if isDatePicker {
            if date.addingTimeInterval(86400.0) <= currentDate {
                dateTimePicker.optionCalendarFontColorFutureDates = UIColor.lightGray
                return false
            }
            return true
        }else
        {
            
//            if self.selectedDate != nil && self.selectedTime!.toString(format: "HH").compare(currentDate.toString(format: "HH")) == .orderedAscending{
//
//            }
        }
   
           return true
        
    }
    
}
