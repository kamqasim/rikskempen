//
//  SettingCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/12/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var nameLbl : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
