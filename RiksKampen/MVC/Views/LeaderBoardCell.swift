//
//  LeaderBoardCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/6/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class LeaderBoardCell: UITableViewCell {

    @IBOutlet weak var firstProgressView: UIView!
    @IBOutlet weak var secondProgressView: UIView!
    @IBOutlet weak var thirdProgressView: UIView!
    
    @IBOutlet weak var firstRoundName: UIButton!
    @IBOutlet weak var secondRoundName: UIButton!
    @IBOutlet weak var thirdRoundName: UIButton!
    
    @IBOutlet weak var firstNameLbl: UILabel!
    @IBOutlet weak var secondNameLbl: UILabel!
    @IBOutlet weak var thirdNameLbl: UILabel!
    
    @IBOutlet weak var firstNumberLbl: UILabel!
    @IBOutlet weak var secondNumberLbl: UILabel!
    @IBOutlet weak var thirdNumberLbl: UILabel!
    
    @IBOutlet weak var firstTimeLbl: UILabel!
    @IBOutlet weak var secondTimeLbl: UILabel!
    @IBOutlet weak var thirdTimeLbl: UILabel!
    
    @IBOutlet weak var addFriendBtn: UIButton!
    @IBOutlet weak var chalangeMessageLbl: UILabel!
    
    @IBOutlet weak var likeUnlikeBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
