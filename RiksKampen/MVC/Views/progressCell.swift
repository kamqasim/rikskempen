//
//  progressCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/9/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class progressCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var img0: UIImageView!
     @IBOutlet weak var img1: UIImageView!
     @IBOutlet weak var img2: UIImageView!
     @IBOutlet weak var img3: UIImageView!
     @IBOutlet weak var img4: UIImageView!
     @IBOutlet weak var img5: UIImageView!
     @IBOutlet weak var img6: UIImageView!
     @IBOutlet weak var img7: UIImageView!
    
    
    @IBOutlet weak var progressBar : UIProgressView!
    @IBOutlet weak var caloriesLbl: UILabel!
    @IBOutlet weak var habitPoints: UILabel!
    @IBOutlet weak var imageleftConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
         var transform : CGAffineTransform = CGAffineTransform(scaleX: 1.0, y: 0.9)
//        progressBar.transform = transform
//        progressBar.layer.cornerRadius = progressBar.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
