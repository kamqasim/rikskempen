//
//  WeeklyCell.swift
//  RiksKampen
//
//  Created by Ikotel on 12/17/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class WeeklyCell: UITableViewCell {

    @IBOutlet weak var CollectionView : UICollectionView!
    
    @IBOutlet weak var dayNumberLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
