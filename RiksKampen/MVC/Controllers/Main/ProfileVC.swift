//
//  ProfileVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/13/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    
    var data = ["Settings","About","Logout"]
    var images : [UIImage] = [UIImage]()
    let currentUser = UserModel.getCurrentUser()
    
    @IBOutlet weak var userLiveChatBtn: UIButton!
    
    @IBOutlet weak var userMessageBtn: UIButton!
    
    @IBOutlet weak var userProfileBtn: UIButton!
    
    @IBOutlet weak var tableView : UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        images = [UIImage(named: "ic_settings")!,UIImage(named: "ic_about")!,UIImage(named: "ic_logout")!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        placingData()
    }
    
    func placingData(){
        
        if let user = currentUser{
            
            DispatchQueue.main.async {
                if let image = user.profileImage.imageFromBase64(){
                    self.userProfileBtn.setImage(image, for: .normal)
                }
            }
        }
    }
}

extension ProfileVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        
        cell.leftImageView.image = images[indexPath.row]
        cell.titleName.text = data[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2{
            print(data[indexPath.row])
            self.logout()
        }
    }
}
