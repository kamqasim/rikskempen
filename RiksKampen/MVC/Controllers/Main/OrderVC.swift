//
//  OrderVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/7/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class OrderVC: UIViewController {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView : UITableView!
    
    
    var completedImages : [UIImage] = [UIImage(named: "ic_product_1")!,UIImage(named: "ic_clock-1")!,UIImage(named: "ic_band")!]
    var completedItemName = ["Bottle Bench","Clock","Band"]
    var completeddetails = ["A safe thing","A clock thing"," A band thing"]
    var cancelImages : [UIImage] = [UIImage(named: "ic_barbell_ench")!]
    var cancelItemName = ["Decline Bench"]
    var canceldetails = ["Decline Barbel Bench Press"]
    var pendingImages : [UIImage] = [UIImage(named: "ic_barbell_ench")!]
    var pendingItemName = ["Decline Bench"]
    var pendingdetails = ["Decline Barbel Bench Press"]
    var status : String = "Pending"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func segmentControlAction(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0: status = "Pending"
        case 1: status = "Completed"
        case 2: status = "Canceled"
        default:print("not there")
        }
        self.tableView.reloadData()
    }
}
extension OrderVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var total = 0
        switch status {
        case "Pending" : total = pendingImages.count
        case "Completed"  : total = completedImages.count
        case "Canceled" : total = cancelImages.count
        default:total = 0
        }
        return total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        cell.selectionStyle = .none
        
        switch status {
        case "Pending" :
            
            cell.itemImageView.image = pendingImages[indexPath.row]
            cell.itemNameLbl.text = pendingItemName[indexPath.row]
            cell.itemDescriptionLbl.text = pendingdetails[indexPath.row]
            
        case "Completed"  :
            
            cell.itemImageView.image = completedImages[indexPath.row]
            cell.itemNameLbl.text = completedItemName[indexPath.row]
            cell.itemDescriptionLbl.text = completeddetails[indexPath.row]
            
        case "Canceled" :
            
            cell.itemImageView.image = cancelImages[indexPath.row]
            cell.itemNameLbl.text = cancelItemName[indexPath.row]
            cell.itemDescriptionLbl.text = canceldetails[indexPath.row]
        default:print("")
        }
        
        cell.likeDislikeBtn.isHidden =  status != "Pending" ? true : false
        cell.itemStatusLbl.text = status
        cell.likeDislikeBtn.layer.cornerRadius = cell.likeDislikeBtn.frame.height / 2
        cell.likeDislikeBtn.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppInternalNetwork.getWebViewVC()
        self.pushToVC(vc: vc)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
        
    }
}
