//
//  WeeklyVideosVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/17/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class WeeklyVideosVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var weekActivities : WeeklyActivities = WeeklyActivities()
    var weekNutrations : WeeklyNutriations = WeeklyNutriations()
    var weekTrainings : WeeklyTrainings = WeeklyTrainings()
    var from : String = ""
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch from {
        case "activities": self.title = "WEEKLY ACTIVITES LIST"
        case "nutriation":  self.title = "WEEKLY NUTRIATIONS LIST"
        case "training":  self.title = "WEEKLY TRAININGS LIST"
        default:
            print("no Item")
        }
    }
}

extension WeeklyVideosVC : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var total = 0
        switch from {
        case "activities": total = weekActivities.daysList.count
        case "nutriation": total = weekNutrations.daysList.count
        case "training": total = weekTrainings.daysList.count
        default:
            print("no Item")
        }
        return total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeeklyCell", for: indexPath) as! WeeklyCell
        
        cell.CollectionView.delegate = self
        cell.CollectionView.dataSource = self
        index = indexPath.row
        switch from {
        case "activities":
            cell.dayNumberLbl.text = "Day \(indexPath.row + 1) Activity"
        case "nutriation":
            cell.dayNumberLbl.text = "Day \(indexPath.row + 1) Nutriation"
        case "training":
            cell.dayNumberLbl.text = "Day \(indexPath.row + 1) Training"
        default:
            print("no Item")
        }
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 185.0
    }
}

extension WeeklyVideosVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var total = 0
        switch from {
        case "activities":
            total = weekActivities.daysList[index].dayActivityList.count
        case "nutriation":
            total = weekNutrations.daysList[index].dayNutriationList.count
        case "training":
            total = weekTrainings.daysList[index].dayTrainingList.count
        default:
            print("no Item")
        }
        return total
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
        switch from {
        case "activities":
            let activity = weekActivities.daysList[index].dayActivityList[indexPath.row]
            cell.videoNameLbl.text = "\(indexPath.row + 1 )  Activity"
            self.imagePlacement(imagePath: imageBaseurl + activity.imagePath, imageView: cell.imageView)
            cell.imageView.image = UIImage(named: "ic_barbell_ench")
        case "nutriation":
            let nutration = weekNutrations.daysList[index].dayNutriationList[indexPath.row]
            cell.videoNameLbl.text = "\(indexPath.row + 1 )  Nutriation"
            self.imagePlacement(imagePath: imageBaseurl + nutration.imagePath, imageView: cell.imageView)
            cell.imageView.image = UIImage(named: "food2")
        case "training":
            cell.videoNameLbl.text = "\(indexPath.row + 1 )  Training"
        default:
            print("no Item")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch from {
        case "activities":
            let vc = AppInternalNetwork.getNutraitionDetailsVC() as! NutraitionDetailsVC
            self.pushToVC(vc: vc)
        case "nutriation":
            print("nutriation")
            let vc = AppInternalNetwork.getNutraitionDetailsVC() as! NutraitionDetailsVC
            self.pushToVC(vc: vc)
            
        case "training":
            print("training")
            let vc = AppInternalNetwork.getVideoDetailsVC() as! VideoDetailsVC
            self.pushToVC(vc: vc)
        default:
            print("no Item")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = 100 //Display Three elements in a row.
        collectionViewSize.height = 165
        return collectionViewSize
    }
}
