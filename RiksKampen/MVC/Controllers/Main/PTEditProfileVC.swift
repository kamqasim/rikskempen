//
//  PTEditProfileVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/7/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift

class PTEditProfileVC: UIViewController ,WWCalendarTimeSelectorProtocol {
    
    @IBOutlet weak var usernamTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
 
    let realm = try! Realm()
    let user = UserModel()
    let currentUser = UserModel.getCurrentUser()
    var selectedImage = ""
    var liberary  = false
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.ageTextField.delegate = self
        self.setUpUI()
        if liberary{
            
        }else{
            self.liberary = false
            self.placingData()
        }
    }
    
    func placingData(){
        
        
        if let user = currentUser{
            
            self.emailTextField.text = user.email
            self.usernamTextField.text = user.firstName
            self.addressTextField.text = user.address
            self.ageTextField.text = user.dateOfBirth
            self.heightTextField.text = user.role?.height
            self.weightTextField.text = user.role?.weight
            
            DispatchQueue.main.async {
                self.profileImage.image = user.profileImage.imageFromBase64()
            }
            self.emailTextField.isUserInteractionEnabled = false
        }
    }
    
    func setUpUI(){
        
        self.hideKeyboardWhenTappedAround()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.profileImage.addGestureRecognizer(tap)
        self.profileImage.isUserInteractionEnabled = true
    }
    
    func addTapGestureOnImage(imageView: UIImageView){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imageView.addGestureRecognizer(tap)
        imageView.isUserInteractionEnabled = true
    }
    
    
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        CameraHandler.shared.showActionSheet(vc: self)
        CameraHandler.shared.imagePickedBlock = { (image) in
            /* get your image here */
            self.liberary = true
            self.profileImage.image = image
            
            self.selectedImage = image.base64(format: .jpeg(0.50))!
        }
    }
    
    func validateInputs(email:String ) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        //
        //        if !self.isValidPassword(testStr: password) {
        //            return "The Password Must Be At Least 6 Characters"
        //        }
        return ""
    }
    
    func loginRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [usernamTextField.text!,emailTextField.text!], stringErrors: ["Please Enter UserName","Please Enter Email"])
        
        if error == ""{
            
            error = validateInputs(email: emailTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = [
            "name": "\(usernamTextField.text!)","email":"\(emailTextField.text!)"
            ,"phone":"\(addressTextField.text!)","address":addressTextField.text!] as [String: Any]
        print(params)
        signUpRequest(params: params)
    }
    
    func signUpRequest(params:[String: Any]){
        self.updateLocal()
//        UserModel.sighnUpAPI(params: params) {_,_ in
//            self.updateLocal()
//        }
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        loginRequestValidation()
    }
    
    func updateLocal(){
        
        if self.usernamTextField.text != "" && self.emailTextField.text != ""{
            print(self.selectedImage)
            try! self.realm.write {
                self.user.firstName          = self.usernamTextField.text!
                self.user.email              = self.emailTextField.text!
                self.user.address            = self.addressTextField.text!
                self.user.profileImage           = self.selectedImage
                self.user.dateOfBirth               = ageTextField.text!
                self.user.role?.height            = heightTextField.text!
                self.user.role?.weight            = weightTextField.text!
                self.user.rememberPassword  = true
                self.realm.add(self.user, update: true)
                SVProgressHUD.showSuccess(withStatus: " Profile Updated")
                
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.popBackAction()
    }
    
    func OpenCalender(){
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        present(selector, animated: true, completion: nil)
            let datetime = WWCalenderTimeDelegateHelper(controller: self)
                self.tabBarController?.tabBar.isHidden = true

                datetime.showDatePicker(date: Date()) { (selectedDate) in
                    print(selectedDate)

                }
    
    }

    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        ageTextField.text = date.stringFromFormat("YYYY-MM-dd")
        self.tabBarController?.tabBar.isHidden = false
    }
}

extension PTEditProfileVC : UITextFieldDelegate   {
   
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case ageTextField:
            OpenCalender()
            return false
        default:return true
        }
    }
}
