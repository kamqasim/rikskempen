//
//  LeaderBoardVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/5/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift

class LeaderBoardVC: UIViewController {

    @IBOutlet weak var userProgressView: UIView!
    @IBOutlet weak var firstPositionView: UIView!
    @IBOutlet weak var secondPositionView: UIView!
    @IBOutlet weak var thirdPositionView: UIView!
    @IBOutlet weak var fourthPositionView: UIView!
    @IBOutlet weak var fivePositionView: UIView!
    
    @IBOutlet weak var stepsBtn: UIButton!
    @IBOutlet weak var caloriesBtn: UIButton!
    @IBOutlet weak var starBtn: UIButton!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usertitleImageView: UIImageView!
    @IBOutlet weak var userPointsLbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var fourthImageView: UIImageView!
    @IBOutlet weak var fiveImageView: UIImageView!
    
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var secondName: UILabel!
    @IBOutlet weak var thirdName: UILabel!
    @IBOutlet weak var fourthName: UILabel!
    @IBOutlet weak var fiveName: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var currentUser = UserModel.getCurrentUser()
    
    var progressArray   : [Double]      = [Double]()
    var leftProgress    : CGFloat       = 0.0
    var imageLeading    : [Int]         = [Int]()
    var stepsArray      : [Int]         = [Int]()
    var selectedImage   : UIImage =  UIImage()
    let realm = try! Realm()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Leader Board"
        selectedImage = UIImage(named: "walking shoes")!
        for i in stride(from: 01, to: 100, by: 1) {imageLeading.append(i)}
        for i in stride(from: 01, to: 445678, by: 1) {stepsArray.append(i)}
        for i in stride(from: 00.1, to: 1.0, by: 0.01) {progressArray.append(i)}

        stepsArray = stepsArray.sorted { $0 > $1 }
        progressArray = progressArray.sorted { $0 > $1 }
        imageLeading = imageLeading.sorted { $0 > $1 }
        
        creatingCircle(views: [firstPositionView,secondPositionView,thirdPositionView,fourthPositionView,fivePositionView], imageViews: [firstImageView,secondImageView,thirdImage,fourthImageView,fiveImageView
            ])
        
        placingData()

    }
    
    func placingData(){
        
        if let user = currentUser{
            self.userName.text = user.firstName
            self.userPointsLbl.text = "#11"
            DispatchQueue.main.async {
                self.userImageView.image = user.profileImage.imageFromBase64()
            }
        }
    }
    
    @IBAction func stepsAndCaloriesAction(_ sender: UIButton) {
        if sender.tag == 0{
            
            selectedImage = UIImage(named: "walking shoes")!
            stepsBtn.backgroundColor = UIColor.clear
            stepsBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            caloriesBtn.setTitleColor(UIColor(hexString: "#C8A168"), for: UIControlState.normal)
            caloriesBtn.backgroundColor = UIColor.white
            starBtn.setTitleColor(UIColor(hexString: "#C8A168"), for: UIControlState.normal)
            starBtn.backgroundColor = UIColor.white
            self.usertitleImageView.image =  selectedImage
            self.userPointsLbl.text = "#11"
            self.tableView.reloadData()
            
        }else if sender.tag == 1{
           
            selectedImage = UIImage(named: "fillStarwhite")!
            self.userPointsLbl.text = "#15"
              self.usertitleImageView.image =  selectedImage
            starBtn.backgroundColor = UIColor.clear
            starBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            stepsBtn.setTitleColor(UIColor(hexString: "#C8A168"), for: UIControlState.normal)
            stepsBtn.backgroundColor = UIColor.white
            caloriesBtn.setTitleColor(UIColor(hexString: "#C8A168"), for: UIControlState.normal)
            caloriesBtn.backgroundColor = UIColor.white
            self.tableView.reloadData()
            
        }else if sender.tag == 2{
            self.userPointsLbl.text = "#11"
            selectedImage = UIImage(named: "calories_t")!
              self.usertitleImageView.image =  selectedImage
            caloriesBtn.backgroundColor = UIColor.clear
            caloriesBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            stepsBtn.setTitleColor(UIColor(hexString: "#C8A168"), for: UIControlState.normal)
            stepsBtn.backgroundColor = UIColor.white
            starBtn.setTitleColor(UIColor(hexString: "#C8A168"), for: UIControlState.normal)
            starBtn.backgroundColor = UIColor.white
            self.tableView.reloadData()
            
        }
    }
}

extension LeaderBoardVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProgressCellUpdate", for: indexPath) as! ProgressCellUpdate

        cell.progressViewWidth.constant = cell.frame.size.width - 60.0 - CGFloat((3 * indexPath.row))
        //cell.profileImageContestant.image = selectedImage
        cell.positionNumber.text = "#\(indexPath.row + 1)"
        cell.caloriesAndStepsImageView.image = selectedImage
        
        cell.nameLbl.text = "Emma -\(368763)"
        cell.selectionStyle = .none

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
}
