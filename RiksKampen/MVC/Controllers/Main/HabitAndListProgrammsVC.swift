//
//  HabitAndListProgrammsVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/5/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

extension HomeVC {
    
    func placingData(){
        
        if let user = currentUser{
            self.usernameLbl.text = user.firstName
            DispatchQueue.main.async {
                self.userImageView.image = user.profileImage.imageFromBase64()
            }
        }
    }
}

extension HomeVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var total  = 0
        
        switch tuple {
        case ("activity", 0):
            
            if collectionView == CollectionView{
                total = activityWeeklyList.count
                print(" Activity \(total)")
            }
        case ("health", 1):
            
            if collectionView == healthCollectionView{
                total = nutrationWeeklyList.count
                print(" Nutriation \(total)")
            }else if collectionView == healthAndFitnessCollectionView {
                total = trainigWeeklyList.count
                print(" Training \(total)")
            }else{
                total = 0
            }
        default:
            print("")
        }
       print(total)
        return total
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HabitsCell", for: indexPath) as! HabitsCell
        
        switch tuple {
        case ("activity", 0):
            
            if collectionView == CollectionView{
                cell.imageView.tag = indexPath.row
//                if indexPath.row == 0{
//                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//                    cell.imageView.isUserInteractionEnabled = true
//                    cell.imageView.addGestureRecognizer(tapGestureRecognizer)
//                }
                
                self.imagePlacement(imagePath: imageBaseurl + activityWeeklyList[indexPath.row].imagePath, imageView: cell.imageView)
                cell.timeLbl.text = activityWeeklyList[indexPath.row].weekName
            }
          
        case ("health", 1):
            self.circleImageView(imageView: cell.imageView)
            if collectionView == healthAndFitnessCollectionView{
                let item = nutrationWeeklyList[indexPath.row]
                self.imagePlacement(imagePath: imageBaseurl + item.imagePath, imageView: cell.imageView)
                cell.timeLbl.text = item.weekName
            }else if collectionView == healthCollectionView {
                let item = trainigWeeklyList[indexPath.row]
                self.imagePlacement(imagePath: imageBaseurl + item.imagePath, imageView: cell.imageView)
                cell.timeLbl.text =  item.weekName
            }else{
                cell.imageView.image = UIImage(named: "yoga")!
                cell.habitNameLbl.text = "egru"
                cell.timeLbl.text = " week"
            }
          
        default:
            print("")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        switch tuple {
        case ("activity", 0):
            let vc = AppInternalNetwork.getActivityDetailsVC() as! ActivityDetailsVC
            vc.weekActivities = activityWeeklyList[indexPath.row]
           self.pushToVC(vc: vc)
        case ("health", 1):
            if collectionView == healthAndFitnessCollectionView{
                let vc = AppInternalNetwork.getWeeklyVideosVC() as! WeeklyVideosVC
                vc.weekNutrations = nutrationWeeklyList[indexPath.row]
                vc.from = "nutriation"
                self.pushToVC(vc: vc)
            }else if collectionView == healthCollectionView {
                let vc = AppInternalNetwork.getWeeklyVideosVC() as! WeeklyVideosVC
                vc.weekTrainings = trainigWeeklyList[indexPath.row]
                vc.from = "training"
                self.pushToVC(vc: vc)
            }
        default:
            print("")
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = 100 //Display Three elements in a row.
        collectionViewSize.height = 145
        return collectionViewSize
    }
}
