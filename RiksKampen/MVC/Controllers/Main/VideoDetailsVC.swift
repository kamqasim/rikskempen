//
//  VideoDetailsVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/19/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import AVKit
import MMPlayerView

class VideoDetailsVC: UIViewController {
    
    @IBOutlet weak var videoPlayerView : UIImageView!
    @IBOutlet weak var containerVideoView: VideoView!
    
    @IBOutlet weak var timeLbl: UILabel!
    
    var isPlaying : Bool = false
    var volume : Bool = false
    var time : String = "00:00:00"
    var timer : Timer = Timer()
    var seconds = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoPlayer()
        // Do any additional setup after loading the view.
        containerVideoView.addTapGestureRecognizer {
            if !self.isPlaying {
                self.containerVideoView.play()
                self.videoPlayerView.isHidden = true
                self.runTimer()
                self.isPlaying = !self.isPlaying
                print("Is Playing")
            }else{
                self.timer.invalidate()
                self.videoPlayerView.isHidden = false
                self.containerVideoView.pause()
                self.isPlaying = !self.isPlaying
                print("Is Playing")
            }
            
            print("image tapped")
        }
    }
    
    @objc func updateTimer() {
    //This will decrement(count down)the seconds.
             seconds += 1
        
        self.hmsFrom(seconds: Int(seconds)) { hours, minutes, seconds in
            
            let hours = self.getStringFrom(seconds: hours)
            let minutes = self.getStringFrom(seconds: minutes)
            let seconds = self.getStringFrom(seconds: seconds)
            self.timeLbl.text = "\(hours):\(minutes):\(seconds)"
            print("\(hours):\(minutes):\(seconds)")
        }
    }
    
    func runTimer() {
       
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(VideoDetailsVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    func videoPlayer(){
        
        containerVideoView.configure(url: "http://riks.pt2019.ae/riksAssets/videos/RK1small.mp4")
        containerVideoView.isLoop = true
    }
    
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func getStringFrom(seconds: Int) -> String {
        
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
    
    func fullScreen(){
        let url = URL(fileURLWithPath: "http://riks.pt2019.ae/riksAssets/videos/RK1small.mp4")
        let player = AVPlayer(url: url)
        let vc = AVPlayerViewController()
        vc.player = player
        
        present(vc, animated: true) {
            vc.player?.play()
        }
    }
    
    @IBAction func fullViewPlay(_ sender: Any) {
        
    }
    @IBAction func StopAndPlay(_ sender: Any) {
        self.containerVideoView.pause()
    }
    @IBAction func volume(_ sender: Any) {
        
    }
    
    
}
