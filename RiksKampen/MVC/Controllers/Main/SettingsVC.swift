//
//  SettingsVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/12/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

 var data = ["Privacy Policy","Terms and conditions","Logout"]
    fileprivate var imagePicker = ImagePicker()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        
    }
    
    fileprivate func presentImagePicker(sourceType: UIImagePickerControllerSourceType) {
        imagePicker.controller.sourceType = sourceType
        DispatchQueue.main.async {
            self.present(self.imagePicker.controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func presentPickerAndRequestPermission(_ sender: UIButton) {
        presentImagePicker(sourceType: .savedPhotosAlbum)
    }
    
    @IBAction func requestPermissionAndPresentPicker(_ sender: UIButton) {
        imagePicker.galleryAsscessRequest()
    }
    
    @IBAction func cameraButtonTapped(_ sender: Any) {
        imagePicker.cameraAsscessRequest()
    }
}
extension SettingsVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        
        cell.nameLbl.text = data[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2{
        print(data[indexPath.row])
            self.logout()
            SignUpVC.signUp = false
        }
    }
}
