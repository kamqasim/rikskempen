//
//  WebViewVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/17/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import Foundation
import WebKit
class WebViewVC: UIViewController  , WKNavigationDelegate{

    
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "RiksKampen"
        webView = WKWebView()
        webView.navigationDelegate = self
        self.view = webView
        
        let url = URL(string: "http://rikskampen.dubaisoftwaresolutions.com/index.php?route=product/category&path=63_59")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true

    }
}
