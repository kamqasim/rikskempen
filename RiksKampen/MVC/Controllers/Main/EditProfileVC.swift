//
//  EditProfileVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/7/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import SVProgressHUD
import DropDown

class EditProfileVC: UIViewController , UITextFieldDelegate {

   
    @IBOutlet weak var usernamTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    // DropDown
     var space = "       "
    let ageDropDwon = DropDown()
    let heightDropDwon = DropDown()
    let weightDropDwon = DropDown()
    // DropDown DataSource
    
    var ageArray : [String] = [String]()
    var heightArray : [String] = [String]()
    var weightArray : [String] = [String]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpUI()
    }
    
    func setUpUI(){
        
        ageArray = Array(0...150).map{ String(repeating: " ", count: 28) + String($0)}
        heightArray = Array(0...210).map{String(repeating: " ", count: 28) + String($0) + "    cm"}
        weightArray = Array(0...500).map{String(repeating: " ", count: 28) + String($0) + "    kg"}
        self.hideKeyboardWhenTappedAround()
        DropDown.startListeningToKeyboard()
        usernamTextField.text = "Muhammad"
        emailTextField.text = "kamqasim1@gmail.com"
        heightTextField.text = "00971568973908"
        addressTextField.text = "Silicon Oasis ,Dubai,UAE"
        setUpDropDown(textfield: ageTextField, dataSource: ageArray, dropdown: ageDropDwon)
        setUpDropDown(textfield: heightTextField, dataSource: heightArray, dropdown: heightDropDwon)
        setUpDropDown(textfield: weightTextField, dataSource: weightArray, dropdown: weightDropDwon)
        //showAttachmentActionSheet()
    }
    
    func validateInputs(email:String) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        return ""
    }
    
    func loginRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [usernamTextField.text!,emailTextField.text!,ageTextField.text!,heightTextField.text!,weightTextField.text! , addressTextField.text!], stringErrors:  ["Please Enter UserName","Please Enter Email", "Please Enter Age", "Please Enter Height","Please Enter Weight","Please Enter Address"])
        
        if error == ""{
            
            error = validateInputs(email: emailTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = [
            "name": "\(usernamTextField.text!)","email":"\(emailTextField.text!)"
            ,"phone":"\(ageTextField.text!)","password":heightTextField.text!,"address":addressTextField.text!] as [String: Any]
        print(params)
        signUpRequest(params: params)
    }
    
    
    func signUpRequest(params:[String: Any]){
        UserModel.sighnUpAPI(params: params) {_,_ in
            print("Success")
            
            let vc = AppInternalNetwork.getAppTabBarController() as! AppTabBarController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func SettingsButtonAction(_ sender: Any) {
        let vc = AppInternalNetwork.getSettingsVC() as! SettingsVC
        self.pushToVC(vc: vc)
    }
    
    
    @IBAction func signUpAction(_ sender: Any) {
        loginRequestValidation()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField  {
        case ageTextField:
            view.endEditing(false)
            ageDropDwon.show()
        case heightTextField:
            view.endEditing(false)
            heightDropDwon.show()
        case weightTextField:
            view.endEditing(false)
            weightDropDwon.show()
        default:
            print("")
        }
    }
}

