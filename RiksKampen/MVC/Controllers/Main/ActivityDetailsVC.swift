//
//  ActivityDetailsVC.swift
//  RiksKampen
//
//  Created by Ikotel on 1/2/19.
//  Copyright © 2019 KamsQue. All rights reserved.
//

import UIKit

class ActivityDetailsVC: UIViewController {

    
    @IBOutlet weak var tableView : UITableView!
    
    
    var weekActivities : WeeklyActivities = WeeklyActivities()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "WEEKLY ACTIVITES LIST"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
}
extension ActivityDetailsVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return weekActivities.daysList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        
        cell.selectionStyle = .none
        // cell.itemImageView.image = activit
        cell.itemNameLbl.text = "DAY \(indexPath.row + 1)"
        cell.itemDescriptionLbl.text = "Address:   Dubai, UAE"
        cell.itemStatusLbl.text = "Calories:   \(Int.random(in: 0 ... 1000))"
        cell.likeDislikeBtn.layer.cornerRadius = cell.likeDislikeBtn.frame.height / 2
        cell.priceLbl.text = "Steps:   \(Int.random(in: 0 ... 100000))"
        cell.itemImageView.image = UIImage(named: "ic_barbell_ench")
        cell.likeDislikeBtn.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppInternalNetwork.getWebViewVC()
        self.pushToVC(vc: vc)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
