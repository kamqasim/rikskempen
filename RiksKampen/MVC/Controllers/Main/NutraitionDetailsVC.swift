//
//  NutraitionDetailsVC.swift
//  RiksKampen
//
//  Created by Ikotel on 1/2/19.
//  Copyright © 2019 KamsQue. All rights reserved.
//

import UIKit
import Charts

class NutraitionDetailsVC: UIViewController {

    @IBOutlet weak var tableView : UITableView!
 
    @IBOutlet weak var chartsView: PieChartView!
    
    var completedImages : [UIImage] = [UIImage(named: "food1")!,UIImage(named: "food2")!,UIImage(named: "food3")!]
    var completedItemName = ["BreakFast","Lunch","Dinner"]
    var completeddetails = ["A healthy food thing","A food thing"," A health thing"]
    var status : String = "Calories"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        setupPieCharts()
    }
    
    func setupPieCharts(){
        
        chartsView.chartDescription?.enabled = true
        chartsView.drawHoleEnabled = true
        chartsView.rotationAngle = 0
        chartsView.rotationEnabled = true
        chartsView.isUserInteractionEnabled = true
        
        chartsView.usePercentValuesEnabled = true
        chartsView.drawSlicesUnderHoleEnabled = true
//        chartsView.holeRadiusPercent = 0.58
//        chartsView.transparentCircleRadiusPercent = 0.61
        chartsView.chartDescription?.enabled = true
//        chartsView.setExtraOffsets(left: 5, top: 10, right: 5, bottom: 5)
        
        chartsView.drawCenterTextEnabled = true
      
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "RiksKampen \n Calories Chart")
       
        centerText.addAttributes([.font : UIFont(name: "HelveticaNeue-Light", size: 13)!,
                                  .foregroundColor : UIColor.gray], range: NSRange(location: 14, length: centerText.length - 17))
        centerText.addAttributes([.font : UIFont(name: "HelveticaNeue-Light", size: 11)!,
                                  .foregroundColor : UIColor.gray], range: NSRange(location: centerText.length - 19, length: 19))
        chartsView.centerAttributedText = centerText;
        chartsView.entryLabelColor = NSUIColor.black
        chartsView.drawHoleEnabled = true
        chartsView.rotationAngle = 0
        chartsView.rotationEnabled = true
        chartsView.highlightPerTapEnabled = true
        chartsView.usePercentValuesEnabled = true
        chartsView.transparentCircleColor = NSUIColor.lightGray

        var entries : [PieChartDataEntry] = Array()
        entries.append(PieChartDataEntry(value: 40.0, label: "Fat"))
        entries.append(PieChartDataEntry(value: 30.0, label: "Calories"))
        entries.append(PieChartDataEntry(value: 30.0, label: "Protien"))
        
        let dataSet = PieChartDataSet(values: entries, label: "")
        let c1 = NSUIColor(hexString: "FF712E")
        let c2 = NSUIColor(hexString: "9E9B2E")
        let c3 = NSUIColor(hexString: "509788")
       
        
        dataSet.colors = [c1,c2,c3]
        dataSet.drawValuesEnabled = true
        dataSet.entryLabelColor = NSUIColor.black
        dataSet.valueLineColor = NSUIColor.black
        dataSet.sliceSpace = 2.0
        chartsView.data = PieChartData(dataSet: dataSet)
    }
  
}
extension NutraitionDetailsVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return completedImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        cell.selectionStyle = .none
        cell.itemImageView.image = completedImages[indexPath.row]
        cell.itemNameLbl.text = completedItemName[indexPath.row]
        cell.itemDescriptionLbl.text = completeddetails[indexPath.row]
        
       cell.priceLbl.text = "Serving"
        cell.itemStatusLbl.text = status
        cell.likeDislikeBtn.layer.cornerRadius = cell.likeDislikeBtn.frame.height / 2
        cell.likeDislikeBtn.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppInternalNetwork.getWebViewVC()
        self.pushToVC(vc: vc)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
        
    }
}
