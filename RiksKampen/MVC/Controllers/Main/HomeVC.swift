//
//  HomeVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/5/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import SwiftyJSON
import CircleProgressView
import SwiftyJSON
import RealmSwift
import AlamofireImage
import CoreLocation

class HomeVC: UIViewController , LocationServiceDelegate , HealthServiceDelegate {
    
    
    @IBOutlet weak var todaysImageView: UIImageView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var piclocationLbl: UILabel!
    @IBOutlet weak var todaysActivityImageView: UIImageView!
    
    @IBOutlet weak var timeLbl: UILabel!
    
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var nutriationTrainingView: UIView!
    @IBOutlet weak var CollectionView : UICollectionView!
    
    @IBOutlet weak var progressView: CircleProgressView!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userPointsLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    
    @IBOutlet weak var healthCollectionView: UICollectionView!
    @IBOutlet weak var healthAndFitnessCollectionView: UICollectionView!
    @IBOutlet weak var stressReliefCollectionView: UICollectionView!
    
    let manager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    var locality = ""
    var administrativeArea = ""
    var country = ""
    
    @IBOutlet weak var stepsLbl: UILabel!
    weak var currentViewController: UIViewController?
    var images = [UIImage]()
    var address :String = ""
    
    var activityWeeklyList: [WeeklyActivities] = [] {
        didSet {
            self.CollectionView.delegate = self
            self.CollectionView.dataSource = self
            self.CollectionView.reloadData()
        }
    }
    
    var nutrationWeeklyList: [WeeklyNutriations] = [] {
        didSet {
            self.healthCollectionView.delegate = self
            self.healthCollectionView.dataSource = self
            self.healthCollectionView.reloadData()
        }
    }
    
    var trainigWeeklyList: [WeeklyTrainings] = [] {
        didSet {
            self.healthAndFitnessCollectionView.delegate = self
            self.healthAndFitnessCollectionView.dataSource = self
            self.healthAndFitnessCollectionView.reloadData()
        }
    }
    
    static var address: String = "" {
        didSet {
            
        }
    }
    
    var tuple  = ("activity" , 0)
    var currentUser = UserModel.getCurrentUser()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        LocationSingleton.sharedInstance.startUpdatingLocation()
        LocationSingleton.sharedInstance.delegate = self
        HealthKitManager.sharedInstance.delegate = self
        
        segmentControl.selectedSegmentIndex = 0
        getingData()
        progressView.progress = 0.8
        progressView.trackFillColor = UIColor(hexString: "#94DE97")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(userImageTapped(tapGestureRecognizer:)))
        self.todaysActivityImageView.isUserInteractionEnabled = true
        self.todaysActivityImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func userImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        if tappedImage.tag == 0{
            CameraHandler.shared.showActionSheet(vc: self)
            CameraHandler.shared.imagePickedBlock = { (image) in
                /* get your image here */
                //self.liberary = true
                tappedImage.image = image
                
                //tappedImage = image.base64(format: .jpeg(0.50))!
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // self.getAddress()
        
        placingData()
        
    }
    
    func tracingHealthUpdate(steps: String, distance: String, floorsAscended: String, floorsDescended: String) {
        print(steps)
        DispatchQueue.main.async {
            self.stepsLbl.text = steps
        }
    }
    
    func tracingLocation(currentLocation: CLLocation) {
        print(currentLocation.coordinate.latitude)
        let addresss = getAddressFromLatLon(pdblLatitude: "\(currentLocation.coordinate.latitude)", withLongitude:  "\(currentLocation.coordinate.longitude)")
    }
    
    func tracingLocationDidFailWithError(error: NSError) {
        print(error)
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) -> String {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        var addressString : String = ""
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    self.piclocationLbl.text = addressString
                    
                    print(addressString)
                }
        })
        
        return addressString
    }
    
    
    
    func getingData(){
        if let user = UserModel.getCurrentUser(){
            if let thisActivities = user.activiteis{
                thisActivities.weekList.forEach({ (week) in
                    activityWeeklyList.append(week)
                    print(" Activity \(week.weekName)")
                })
            }
        }
        
        if let user = UserModel.getCurrentUser(){
            if let thisnutriations = user.nutrations{
                
                thisnutriations.weekList.forEach({ (week) in
                    nutrationWeeklyList.append(week)
                    print(" Nutration \(week.weekName)")
                    
                })
            }
            
            if let thistrainings = user.trainings{
                thistrainings.weekList.forEach({ (week) in
                    trainigWeeklyList.append(week)
                    print(" Training \(week.weekName)")
                })
            }
            
        }
    }
    
    //    func add(asChildViewController viewController: UIViewController) {
    //        self.containerView.addSubview(viewController.view)
    //        viewController.view.frame = containerView.bounds
    //        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    //        viewController.didMove(toParentViewController: self)
    //    }
    
    //   func remove(asChildViewController viewController: UIViewController) {
    //        viewController.willMove(toParentViewController: nil)
    //        viewController.view.removeFromSuperview()
    //        viewController.removeFromParentViewController()
    //    }
    //
    
    func changeView() {
        if segmentControl.selectedSegmentIndex == 0 {
            tuple = ("activity",0)
            
            self.activityView.isHidden = false
            self.nutriationTrainingView.isHidden = true
            getingData()
        } else if segmentControl.selectedSegmentIndex == 1{
            tuple = ("health",1)
            getingData()
            self.activityView.isHidden = true
            self.nutriationTrainingView.isHidden = false
        }
    }
    
    @IBAction func segmentConrolAction(_ sender: UISegmentedControl) {
        changeView()
        print(segmentControl.selectedSegmentIndex)
    }
    
}
