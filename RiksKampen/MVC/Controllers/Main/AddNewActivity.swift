//
//  Addnewactivity.swift
//  RiksKampen
//
//  Created by Ikotel on 12/12/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import CircleProgressView
import SwiftyJSON
import RealmSwift
import AlamofireImage

extension HomeVC:   UIImagePickerControllerDelegate
,UINavigationControllerDelegate{
    
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func pushToController(item : WeeklyActivities){
  
        let vcs = AppInternalNetwork.getWeeklyVideosVC() as! WeeklyVideosVC
        vcs.weekActivities = item
        vcs.from = "activity"
        self.pushToVC(vc: vcs)
    }
    
}
extension UIViewController {
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}
//extension HomeVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        return weeklyList.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddNewActivityCell", for: indexPath) as! AddNewActivityCell
//        cell.imageView.tag = indexPath.row
//
//        if indexPath.row == 0{
//            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//            cell.contentView.isUserInteractionEnabled = true
//            cell.contentView.addGestureRecognizer(tapGestureRecognizer)
//        }
//
//        self.imagePlacement(imagePath: imageBaseurl + weeklyList[indexPath.row].imagePath, imageView: cell.imageView)
//        cell.timeLbl.text = weeklyList[indexPath.row].weekName
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let item = weeklyList[indexPath.row]
//
//        self.pushToController(item: item)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        var collectionViewSize = collectionView.frame.size
//        collectionViewSize.width = 100 //Display Three elements in a row.
//        collectionViewSize.height = 137
//        return collectionViewSize
//    }
//}

protocol ImagePickerDelegate {
    func imagePickerDelegate(canUseCamera accessIsAllowed:Bool, delegatedForm: ImagePicker)
    func imagePickerDelegate(canUseGallery accessIsAllowed:Bool, delegatedForm: ImagePicker)
    func imagePickerDelegate(didSelect image: UIImage, imageName:String, delegatedForm: ImagePicker)
    func imagePickerDelegate(didCancel delegatedForm: ImagePicker)
}

extension ImagePickerDelegate {
    func imagePickerDelegate(canUseCamera accessIsAllowed:Bool, delegatedForm: ImagePicker) {}
    func imagePickerDelegate(canUseGallery accessIsAllowed:Bool, delegatedForm: ImagePicker) {}
}
