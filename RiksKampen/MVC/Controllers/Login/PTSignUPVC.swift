//
//  PTSignUP.swift
//  RiksKampen
//
//  Created by Ikotel on 12/5/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift

class PTSignUPVC: UIViewController  {
    
    @IBOutlet weak var firstnamTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passowrdTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    let user = UserModel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Signup"
        
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.isNavigationBarHidden = false;
    }
    
    func setUpUI(){
        
        self.hideKeyboardWhenTappedAround()
        firstnamTextField.text = "Muhammad"
        lastnameTextField.text = "Qasim"
        emailTextField.text = "kamqasim1@gmail.com"
        passowrdTextField.text = "Rickskampen39))"
        confirmPasswordTextField.text = "Rikskampen39))"
    }
    
    func validateInputs(email:String ,password: String) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        
        if !self.isValidPassword(testStr:  passowrdTextField.text!) || !self.isValidPassword(testStr: self.confirmPasswordTextField.text!){
            return "The Password Must Be At Least 6 Characters"
        }
        
        if self.confirmPasswordTextField.text != self.passowrdTextField.text!{
            return "The Password Must Be Matched"
        }
        return ""
    }
    
    func loginRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [firstnamTextField.text!,emailTextField.text!,passowrdTextField.text!,confirmPasswordTextField.text!], stringErrors: ["Please Enter UserName","Please Enter Email", "Please Enter Confirm Password", "Please Enter Password"])
        
        if error == ""{
            
            error = validateInputs(email: emailTextField.text!, password:  passowrdTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = [
            "firstname": "\(firstnamTextField.text!)","lastname":"\(lastnameTextField.text!)","email":"\(emailTextField.text!)","password":confirmPasswordTextField.text!] as [String: Any]
        print(params)
        signUpRequest(params: params)
    }
    
    func signUpRequest(params:[String: Any]){
        UserModel.sighnUpAPI(params: params) {status,token in
       
            SVProgressHUD.showSuccess(withStatus: "")
//            let realm = try! Realm()
//            if self.emailTextField.text != "" && self.passowrdTextField.text != ""{
//                //try! realm.commitWrite()
//                try! realm.write {
//                    self.user.email            = self.emailTextField.text!
//                    self.user.password         = self.passowrdTextField.text!
//                    self.user.rememberPassword = true
//                    realm.add(self.user, update: true)
//                }
//            }
            
            print(status , token)
//            let realm = try! Realm()
//            try! realm.write {
//                self.user.authToken            = token
//                //self.user.rememberPassword = true
//                realm.add(self.user, update: true)
//            }
            
            let vc = AppInternalNetwork.getLoginVC() as! LoginVC
            self.pushToVC(vc:vc)
        }
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        loginRequestValidation()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.popVc()
    }
    
}

