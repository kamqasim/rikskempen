//
//  VerfiCodeVC.swift
//  RiksKampen
//
//  Created by Productions on 11/11/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD

class VerfiCodeVC: UIViewController {

    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    
    var verifiedCode : String = ""
    private var code : String = ""
    var email : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firstTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        secondTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        thirdTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        fourthTextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        firstTextField.becomeFirstResponder()
    }

    @IBAction func popVCAction(_ sender: Any) {
        self.popVc()
    }
    
    
    func sendEmailRequest(params:[String: Any]){
        UserModel.sendEmailAPI(params: params) { (verifyCode) in
            self.verifiedCode = verifyCode
           
            self.firstTextField.text = ""
            self.secondTextField.text = ""
            self.thirdTextField.text = ""
            self.fourthTextField.text = ""
            self.firstTextField.resignFirstResponder()
            print(verifyCode)
            print("Success")
        }
    }
    
    @IBAction func resendCode(_ sender: Any) {
        let params = [
            "email": "\(email)"
            ] as [String: Any]
        sendEmailRequest(params:params)
    }
    
}
extension VerfiCodeVC: UITextFieldDelegate {
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let  text = textField.text
        
        if text?.utf16.count == 1{
            
            switch textField {
            case firstTextField:
                
                secondTextField.becomeFirstResponder()
            case secondTextField:thirdTextField.becomeFirstResponder()
            case thirdTextField:fourthTextField.becomeFirstResponder()
            case fourthTextField:fourthTextField.resignFirstResponder()
                code = firstTextField.text! + secondTextField.text! + thirdTextField.text! + fourthTextField.text!
                
                if code == verifiedCode{
                    print("Success")
                    let vc = AppInternalNetwork.getResetPasswordVC() as! ResetPasswordVC
                    vc.email = self.email
                    self.pushToVC(vc: vc)
                }else{
                    SVProgressHUD.showError(withStatus: "Pin Code Error")
                }
            default:
                break
            }
        }else{
            
        }
    }
    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if !(string == "") {
//            textField.text = string
//            if textField == firstTextField {
//                secondTextField.becomeFirstResponder()
//            }
//            else if textField == secondTextField {
//                thirdTextField.becomeFirstResponder()
//            }
//            else if textField == thirdTextField {
//                fourthTextField.becomeFirstResponder()
//
//            }else if textField == fourthTextField{
//
//            }
//            return false
//        }
//        return true
//    }
//
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if (textField.text?.count ?? 0) > 0 {
//
//            if textField == fourthTextField{
//                let vc = AppInternalNetwork.getResetPasswordVC()
//                self.pushToVC(vc: vc)
//            }
//        }
//        return true
//    }
}
