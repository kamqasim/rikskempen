//
//  SendEmailVC.swift
//  RiksKampen
//
//  Created by Productions on 11/11/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD

class SendEmailVC: UIViewController {

    @IBOutlet weak var emailTextfield : UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextfield.text = "kamqasim1@gmail.com"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendCodeToEmail(_ sender: Any) {
       requestValidation()
    }
    
    @IBAction func back(_ sender: Any) {
        self.popVc()
    }
    
    func validateInputs(email:String) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        
        return ""
    }
    
    func requestValidation(){
        
        var error = checkFieldStringValues(stringValues: [emailTextfield.text!], stringErrors: ["Please Enter Email"])
        
        if error == ""{
            
            error = validateInputs(email: emailTextfield.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = [
            "email": "\(emailTextfield.text!)"
            ] as [String: Any]
        
        print(params)
        sendEmailRequest(params: params)
    }
    
    func sendEmailRequest(params:[String: Any]){
//        UserModel.sendEmailAPI(params: params) { (verifyCode) in
//            print(verifyCode)
//            print("Success")
//            
//            let vc = AppInternalNetwork.getVerfiCodeVC() as! VerfiCodeVC
//            vc.verifiedCode = verifyCode
//            vc.email = self.emailTextfield.text!
//            self.pushToVC(vc: vc)
//        }
        let vc = AppInternalNetwork.getVerfiCodeVC() as! VerfiCodeVC
        vc.verifiedCode = "1234"
        vc.email = self.emailTextfield.text!
        self.pushToVC(vc: vc)
    }
}
