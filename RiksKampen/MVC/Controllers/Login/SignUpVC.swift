//
//  SignUpVC.swift
//  RiksKampen
//
//  Created by Productions on 11/4/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD
import DropDown
import RealmSwift

class SignUpVC: UIViewController  , UITextFieldDelegate{
    
    
    @IBOutlet weak var usernamTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var passowrdTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    // DropDown
    
    let ageDropDwon = DropDown()
    let heightDropDwon = DropDown()
    let weightDropDwon = DropDown()
    // DropDown DataSource
    
    var ageArray : [String] = [String]()
    var heightArray : [String] = [String]()
    var weightArray : [String] = [String]()
    static var signUp : Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false;
    }
    
    func setUpUI(){
        
        self.title = "Signup"
        
        ageArray = Array(0...150).map{ String(repeating: " ", count: 28) + String($0) + "    years"}
        heightArray = Array(0...210).map{String(repeating: " ", count: 28) + String($0) + "    cm"}
        weightArray = Array(0...500).map{String(repeating: " ", count: 28) + String($0) + "    kg"}
        
        self.hideKeyboardWhenTappedAround()
        DropDown.startListeningToKeyboard()
        usernamTextField.text = "Muhammad"
        emailTextField.text = "kamqasim1@gmail.com"
        ageTextField.text = "26 years"
        heightTextField.text = "172 cm"
        weightTextField.text = "55 kg"
        addressTextField.text = "Silicon Oasis ,Dubai,UAE"
        passowrdTextField.text = "Rickskampen39))"
        confirmPasswordTextField.text = "Rikskampen39))"
        setUpDropDown(textfield: ageTextField, dataSource: ageArray, dropdown: ageDropDwon)
        setUpDropDown(textfield: heightTextField, dataSource: heightArray, dropdown: heightDropDwon)
        setUpDropDown(textfield: weightTextField, dataSource: weightArray, dropdown: weightDropDwon)
    }
    
    func validateInputs(email:String ,password: String) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        
        if !self.isValidPassword(testStr: password) {
            return "The Password Must Be At Least 6 Characters"
        }
        return ""
    }
    
    func loginRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [usernamTextField.text!,emailTextField.text!,passowrdTextField.text!,confirmPasswordTextField.text!], stringErrors: ["Please Enter UserName","Please Enter Email", "Please Enter Confirm Password", "Please Enter Password"])
        
        if error == ""{
            
            error = validateInputs(email: emailTextField.text!, password:  passowrdTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = [
            "name": "\(usernamTextField.text!)","email":"\(emailTextField.text!)"
            ,"phone":"\(ageTextField.text!)","password":confirmPasswordTextField.text!,"address":addressTextField.text!] as [String: Any]
        print(params)
        signUpRequest(params: params)
    }
    
    
    func signUpRequest(params:[String: Any]){
        
        UserModel.sighnUpAPI(params: params) {_,_ in
            print("Success")
            SVProgressHUD.showSuccess(withStatus: "")
            let realm = try! Realm()
            
            if SignUpVC.signUp {
                SVProgressHUD.showSuccess(withStatus: "User already Registered  \n Please Login")
                self.popVc()
            }else{
                if self.usernamTextField.text != "" && self.passowrdTextField.text != ""{
                    try! realm.commitWrite()
                    try! realm.write {

                        SignUpVC.signUp = true
                    }
                }
            }
            let vc = AppInternalNetwork.getLoginVC() as! LoginVC
            self.pushToVC(vc:vc)
        }
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        loginRequestValidation()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.popVc()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch textField  {
        case ageTextField:
            view.endEditing(false)
            ageDropDwon.show()
        case heightTextField:
            view.endEditing(false)
            heightDropDwon.show()
        case weightTextField:
            view.endEditing(false)
            weightDropDwon.show()
        default:
            print("")
        }
    }
}
