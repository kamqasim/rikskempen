//
//  BeforeLoginVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/5/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit

class BeforeLoginVC: UIViewController {

    @IBOutlet weak var LoginBtn : UIButton!
    @IBOutlet weak var SignUpBtn : UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
        let user = UserModel.getCurrentUser()
        if let user = user{
            if user.rememberPassword == true{
                let tabBar = AppInternalNetwork.getAppTabBarController() as! AppTabBarController
                self.present(tabBar, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        let vc = AppInternalNetwork.getLoginVC() as! LoginVC
        vc.title = "Contestent SignUp"
        self.pushToVC(vc: vc)
    }
    
    @IBAction func SignUpAction(_ sender: Any) {
        let vc = AppInternalNetwork.getPTSignUPVC() as! PTSignUPVC
        self.pushToVC(vc: vc)
    }
}
