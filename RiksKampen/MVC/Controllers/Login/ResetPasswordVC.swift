//
//  ResetPasswordVC.swift
//  RiksKampen
//
//  Created by Productions on 11/11/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD

class ResetPasswordVC: UIViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var email : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func resetPasswordAction(_ sender: Any) {
        // vc.navigationController?.isNavigationBarHidden = false
        updatePasswordRequestValidation()
    }
    
    @IBAction func back(_ sender: Any) {
        self.popVc()
    }
    
    func validateInputs(password:String ,confirmPassword: String ) -> String {
        
        
        
        if !self.isValidPassword(testStr: password) {
            return "The Password Must Be At Least 6 Characters"
        }
        
        if !self.isValidPassword(testStr: confirmPassword) {
            return "The Password Must Be At Least 6 Characters"
        }
        
        if password != confirmPassword{
            return "Password and Confirm Password Must Be Matched"
        }
        return ""
    }
    
    func updatePasswordRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [passwordTextField.text!,confirmPasswordTextField.text!], stringErrors: ["Please Enter Password", "Please Enter Confirm Password"])
        
        if error == ""{
            
            error = validateInputs(password: passwordTextField.text!, confirmPassword:  confirmPasswordTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = [
            "email": "\(email)","pass":"\(passwordTextField.text!)"
            ] as [String: Any]
        
        print(params)
        loginRequest(params: params)
    }
    
    func loginRequest(params:[String: Any]){
        UserModel.resetPasswordAPI(params: params) {
            print("Success")
            self.popBack(toControllerType: LoginVC.self)
        }
    }
}
