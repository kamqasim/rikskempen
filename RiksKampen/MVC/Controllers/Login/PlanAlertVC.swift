//
//  PlanAlertVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/16/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import DLRadioButton

class PlanAlertVC: UIViewController {

    @IBOutlet weak var prizeLbl : UILabel!
    @IBOutlet weak var ageLimitLbl : UILabel!
    @IBOutlet weak var juniorLbl : UILabel!
    @IBOutlet weak var singleLbl : UILabel!
    @IBOutlet weak var duolLbl : UILabel!
    @IBOutlet weak var backgroundImageView : UIImageView!
    
    @IBOutlet weak var singleBtn: DLRadioButton!
    @IBOutlet weak var doubleBtn: DLRadioButton!
    
    var prize       = ""
    var genration   = ""
    var age         = ""
    var single      = ""
    var double      = ""
    var image       = UIImage()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prizeLbl.text = prize
        self.juniorLbl.text = genration
        self.ageLimitLbl.text = age
        self.singleLbl.isHidden = true
        self.singleBtn.setTitle(single, for: .normal)
        self.doubleBtn.setTitle(double, for: .normal)
        self.duolLbl.isHidden = true
        self.backgroundImageView.image = self.image
    }
    

    @IBAction func TakePlan(_ sender: UIButton) {
        let vc = AppInternalNetwork.getAppTabBarController() as! AppTabBarController
        self.present(vc, animated: true, completion: nil)
    }
    
    
}
