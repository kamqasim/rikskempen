//
//  ViewController.swift
//  RiksKampen
//
//  Created by Productions on 10/29/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift
import FacebookLogin
import FBSDKLoginKit
import GoogleSignIn

var realm = try? Realm()

class LoginVC: UIViewController ,GIDSignInDelegate, GIDSignInUIDelegate {
    
    
    @IBOutlet weak var usernamTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UITextField!
    @IBOutlet weak var signUpBtn: UITextField!
    @IBOutlet weak var fbSighnUpBtn: UIButton!
    @IBOutlet weak var googleSignUpBtn: UIButton!
    
    private var dict : [String : AnyObject]!
    
    var userLists : Results<OtherUserList>!
    var currentUser : UserModel = UserModel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //        try! realm?.write {
        //            realm?.deleteAll()
        //        }
        
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self

        usernamTextField.text = "ayazmuhammad220@gmail.com"
        passwordTextField.text = "ayaz.khan"
        
//        usernamTextField.text = "kamqasim1@gmail.com"
//        passwordTextField.text = "Rickskampen39))"
       
        
        self.hideKeyboardWhenTappedAround()
        googleSignUpBtn.addTarget(self, action: #selector(googleLoginButtonClicked), for: .touchUpInside)
        fbSighnUpBtn.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    @objc func googleLoginButtonClicked() {
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    @objc func loginButtonClicked() {
        
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.email,.publicProfile], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                FBSDKLoginManager().logOut()
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let _):
                self.getFBUserData()
            }
        }
    }
    
    func getFBUserData(){
        
        let parameters = ["fields": "email,picture.type(large),name,gender,age_range,cover,timezone,verified,updated_time,education,religion,friends"]
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    var email = ""
                    var id  = ""
                    
                    if let userData = result as? [String:AnyObject] {
                        if let thisemail = userData["name"] as? String , let thisid = userData["name"] as? String{
                            email = thisemail
                            id = thisid
                            let params = [
                                "email":"\(email)","password":""
                                ,"status":"f"] as [String: Any]
                            
                            print(params)
                            self.loginRequest(params: params)
                        }
                    }
                    print(result!)
                }
            })
        }
    }
    
    func validateInputs(email:String ,password: String ) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        
        if !self.isValidPassword(testStr: password) {
            return "The Password Must Be At Least 6 Characters"
        }
        return ""
    }
    
    func loginRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [usernamTextField.text!,passwordTextField.text!], stringErrors: ["Please Enter UserName", "Please Enter Password"])
        
        if error == ""{
            
            error = validateInputs(email: usernamTextField.text!, password:  passwordTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = [
            "email":"\(usernamTextField.text!)","password":"\(passwordTextField.text!)"] as [String: Any]
        
        print(params)
        loginRequest(params: params)
    }
    
    func loginRequest(params:[String: Any]){
        
//        if let currentuser = UserModel.getCurrentUser() {
//            if usernamTextField.text! == currentuser.email && passwordTextField.text! == currentuser.password{
//
//                 SVProgressHUD.showSuccess(withStatus: "Login Successfully")
////                let vc = AppInternalNetwork.getAppTabBarController() as! AppTabBarController
////                vc.navigationController?.isNavigationBarHidden = false
////                self.present(vc, animated: true, completion: nil)
//
//                let vc = AppInternalNetwork.getIntroductionVC() as! IntroductionVC
//                self.pushToVC(vc: vc)
//            }
//        }else{
//            SVProgressHUD.showError(withStatus: "User Not Found")
//            let vc = AppInternalNetwork.getSignUpVC() as! SignUpVC
//            vc.pushToVC(vc: vc)
//        }
        
                UserModel.loginAPI(params: params) { (apiStaus , token) in
                    if apiStaus{
                        //self.updateUserToken(token: token)
                        print("Success")
                      
                        let vc = AppInternalNetwork.getIntroductionVC() as! IntroductionVC
                        vc.navigationController?.navigationItem.hidesBackButton = true
                        self.pushToVC(vc: vc)

                    }else{
                        let vc = AppInternalNetwork.getSignUpVC() as! SignUpVC
                        vc.pushToVC(vc: vc)
                    }
                }
    }
    
    func updateUserToken(token: String){
        
        try! realm?.write {
            if let user = UserModel.getCurrentUser(){
                user.password = passwordTextField.text!
                
                user.address = "UAE Dubai BusnissBay"
                let dnutriation = Nutriations()
//                dnutriation.nutriationId = "8888"
//                dnutriation.nutriationName = "Sunday"
                let wnutriation = WeeklyNutriations()
//                wnutriation.userId = "00"
//                wnutriation.trainerId  = "9999"
//                wnutriation.nutriationName = "1st Week"
//                wnutriation.nutriationList.append(dnutriation)
//               // user.NutrationList.append(dnutriation)
                user.rememberPassword = true
                user.authToken = token
                realm?.add(user, update: false)
            }
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        self.loginRequestValidation()
    }
    
    @IBAction func forgotPassWordAction(_ sender: Any) {
        
        let vc = AppInternalNetwork.getSendEmailVC() as! SendEmailVC
        self.pushToVC(vc: vc)
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        
        let vc = AppInternalNetwork.getPTSignUPVC() as! PTSignUPVC
        self.pushToVC(vc: vc)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print("Google Sing In didSignInForUser")
        if let error = error {
            print(error.localizedDescription)
            return
        }
        guard let authentication = user.authentication else { return }
        if (error == nil) {
            // Perform any operations on signed in user here.
            if let email = user.profile.email{
                let params = [
                    "email":"\(email)","password":""
                    ,"status":"g"] as [String: Any]
                print(params)
                loginRequest(params: params)
            }
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn?, present viewController: UIViewController?) {
        if let aController = viewController {
            present(aController, animated: true) {() -> Void in }
        }
    }
    
    func sign(_ signIn: GIDSignIn?, dismiss viewController: UIViewController?) {
        dismiss(animated: true) {() -> Void in }
    }
}


