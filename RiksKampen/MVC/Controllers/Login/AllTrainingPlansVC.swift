//
//  AllTrainingPlansVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/16/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController
import DLRadioButton

class AllTrainingPlansVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var prize : [String] = ["1st prize 25,000 kr.","1st prize 50,000 kr.","1st prize 50,000 kr.","1st prize 50,000 kr.","1st prize 1,00,000 kr."]
    var genration : [String] = ["Junior","Vuxen","Senior","Veteran","ELIT"]
    var ages : [String] = ["13-16 years","16-45 years","45-65 years","65-100 years","13-100 years"]
    var single : [String] = ["2495 kr 110/man single","6995 kr 2950/man single","6995 kr 295/man single","4995 kr 215/man single","7995 kr 336/man single"]
    var double : [String] = ["3495 kr 155/man/pers DUO","8995 kr 195/man/pers DUO","8995 kr 195/man/pers DUO","6995 kr 145/man/pers DUO","7995 kr 215/man/pers DUO"]

    var images : [UIImage] = [UIImage]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Traning Plans"
        
        images = [UIImage(named: "one")!,UIImage(named: "two")!,UIImage(named: "three")!,UIImage(named: "four")!,UIImage(named: "five")!]
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }
}
extension AllTrainingPlansVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prize.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlansCell", for: indexPath) as! PlansCell
        
        cell.prizeLbl.text = prize[indexPath.row]
        cell.juniorLbl.text = genration[indexPath.row]
        cell.ageLimitLbl.text = ages[indexPath.row]
        cell.singleLbl.text = single[indexPath.row]
        cell.duolLbl.text = double[indexPath.row]
        cell.backgroundImageView.image = images[indexPath.row]
        switch indexPath.row {
        case 0:print("oo")
//            cell.prizeLbl.textColor = UIColor(red: 82/255, green: 82/255, blue: 187/255, alpha: 1)
        default:
            print("oo")
        }
        cell.backgroundImageView.tag = indexPath.row
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        cell.backgroundImageView.isUserInteractionEnabled = true
        cell.backgroundImageView.addGestureRecognizer(tapGestureRecognizer)
        
        
        return cell
    }
    
    @objc override func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        let prize = self.prize[tappedImage.tag]
        let genration = self.genration[tappedImage.tag]
        let age = self.ages[tappedImage.tag]
        let single = self.single[tappedImage.tag]
        let double = self.double[tappedImage.tag]
        let image = self.images[tappedImage.tag]
        
        self.PlanAlert(prize: prize, genration: genration, age: age, single: single, double: double, image: image)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    func PlanAlert(prize: String,genration: String , age: String , single : String ,double: String ,image : UIImage){
        
        let vc = AppInternalNetwork.getPlanAlertVC() as! PlanAlertVC
        vc.prize = prize
        vc.genration = genration
        vc.age = age
        vc.single = single
        vc.double = double
        vc.image = image
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: vc)
        
        formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
        formSheetController.presentationController?.shouldApplyBackgroundBlurEffect = true;
        formSheetController.presentationController?.contentViewSize = CGSize(width: self.view.frame.width * 0.85, height: self.view.frame.height * 0.8)  // or pass in UILayoutFittingCompressedSize to size automatically with auto-layout
        
        //        formSheetController.presentationController?.portraitTopInset. =
        
        self.present(formSheetController, animated: true, completion: nil)
        formSheetController.view.superview?.frame = CGRect(x:0, y:0, width:self.view.frame.width * 0.9, height:self.view.frame.height * 0.9)
    }
}
