//
//  IntroductionVC.swift
//  RiksKampen
//
//  Created by Ikotel on 12/16/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import RealmSwift

class IntroductionVC: UIViewController {

    @IBOutlet weak var textView : UITextView!
    
    var realm = try? Realm()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Introduction"
   
        try! realm?.write {
            if let user = UserModel.getCurrentUser(){
                user.rememberPassword = true
                user.activiteis?.weekList.forEach({ (week) in
                    print(week)
                })
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false;
        self.textView.contentOffset = CGPoint.zero
    }

    
    @IBAction func skipAction(_ sender: Any) {
        
        let vc = AppInternalNetwork.getAllTrainingPlansVC() as! AllTrainingPlansVC
        vc.navigationItem.backBarButtonItem?.title = " "
        self.pushToVC(vc: vc)
    }
    

}
