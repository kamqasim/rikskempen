//
//  UpdateProfileVC.swift
//  RiksKampen
//
//  Created by Productions on 11/14/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift

class UpdateProfileVC: UIViewController {

   
    @IBOutlet weak var usernamTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
 
    let currentUser = UserModel.getCurrentUser()
     let realm = try! Realm()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Update Profile"
        
        self.hideKeyboardWhenTappedAround()
        self.usernamTextField.isEnabled = false
        self.emailTextField.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setData()
    }
    
    func setData(){
        if let user = currentUser{
            usernamTextField.text = user.firstName
            emailTextField.text = user.email
            //phoneTextField.text = user.phoneNumber
            addressTextField.text = user.address
        }
    }
    
    func validateInputs(email:String ) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        
        return ""
    }
    
    func updateRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [usernamTextField.text!,emailTextField.text!,phoneTextField.text!,addressTextField.text!], stringErrors: ["Please Enter UserName  ","Please Enter Email", "Please Enter PhoneNumber", "Please Enter Address"])
        
        if error == ""{
            
            error = validateInputs(email: emailTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = ["email":"\(emailTextField.text!)"
            ,"phone":"\(phoneTextField.text!)","address":addressTextField.text!] as [String: Any]
        print(params)
        updateProfileRequestApi(params: params)
    }
    
    func setLeftImage(button:UIButton,left:Bool ,image:UIImage?){
        let imageView = UIImageView()
        imageView.image = image
        imageView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        button.imageEdgeInsets = UIEdgeInsets(top: 40, left: 50, bottom: 0, right: 0)
        button.contentHorizontalAlignment = .center
        button.contentMode = .left
    }
    
    func updateProfileRequestApi(params:[String: Any]){
        UserModel.updateProfileAPI(params: params) { (apiStatus) in
            if apiStatus{
                 print("Success")
                if let user = self.currentUser{
                    try! self.realm.write {
                        user.address = self.addressTextField.text!
                       // user.phoneNumber = self.phoneTextField.text!
                        self.realm.add(user, update: true)
                    }
                }
            }else{
                
            }
        }
       
     

    }
    
    @IBAction func signUpAction(_ sender: Any) {
        updateRequestValidation()
    }
    
}
