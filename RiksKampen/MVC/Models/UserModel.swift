//
//  UserModel.swift
//  RiksKampen
//
//  Created by Ikotel on 12/3/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//


import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import SVProgressHUD


class UserModel: Object {
    
    
    override class func primaryKey() -> String? {
        return "email"
    }
    
    @objc dynamic var userid        : String = ""
    @objc dynamic var firstName      : String = ""
    @objc dynamic var lastName      : String = ""
    @objc dynamic var gender      : String = ""
    @objc dynamic var dateOfBirth : String = ""
    @objc dynamic var email         : String = ""
    @objc dynamic var password      : String = ""
    @objc dynamic var address : String = ""
    @objc dynamic var thumbImage  :  String   = ""
    @objc dynamic var profileImage  :  String   = ""
    @objc dynamic var phonenumber   : String = ""
    @objc dynamic var authToken     :  String   = ""
    
    @objc dynamic var isLogin       : Bool = false
    @objc dynamic var rememberPassword : Bool   = false
    @objc dynamic var role  : Role? = Role()
    @objc dynamic var trainings : Trainings? = Trainings()
    @objc dynamic var activiteis : Activities? = Activities()
    @objc dynamic var nutrations : Nutriations? = Nutriations()
    @objc dynamic var orderHistory : OrderHistory? = OrderHistory()
    
    //    "userid": "1",
    //    "firstName": "John",
    //    "lastName": "Smith",
    //    "gender": "man",
    //    "dateOfBirth": "32",
    //    "email": "kamqasim1@gmail.com",
    //    "password": "54762379",
    //    "streetAddress": "21 2nd Street",
    //    "profileImage": "eruyefuyv",
    //    "thumbImage": "eruyefuyv",
    //    "phonenumber": "212 555-1234",
 
    
    func updateModelWithJSON(json:JSON){
        
        self.firstName     = (json["firstName"].stringValue)
        self.lastName   = (json["lastName"].stringValue)
        self.gender     = (json["gender"].stringValue)
        self.dateOfBirth   = (json["dateOfBirth"].stringValue)
        self.email      = (json["email"].stringValue)
        self.password        = (json["password"].stringValue)
        self.address     = (json["address"].stringValue)
        self.thumbImage     = (json["thumbImage"].stringValue)
        self.address    = (json["address"].stringValue)
        self.profileImage  = (json["profileImage"].stringValue)
        self.phonenumber = (json["phonenumber"].stringValue)
        self.authToken      =   (json["authToken"].stringValue)
        
        
        print(self.phonenumber)
        print(self.role?.roleName)
        
        self.addRole(json: json["role"])
        self.addActivity(json: json["Home"]["Activities"])
        self.addTrainings(json: json["Home"]["Trainings"])
        self.addNutriations(json: json["Home"]["Nutriations"])
    }
    
    func addRole(json:JSON){
        
        let thisrole = Role()
        thisrole.updateModelWithJSON(json: json)
        self.role       = thisrole
    }
    
    func addTrainings(json: JSON){
        
        let thisTrainings = Trainings()
        thisTrainings.updateModelWithJSON(json: json)
        self.trainings = thisTrainings
    }
    
    func addNutriations(json: JSON){
        
        let thisNutriations = Nutriations()
        thisNutriations.updateModelWithJSON(json: json)
        self.nutrations = thisNutriations
    }
    
    func addActivity(json: JSON){
        
        let thisActivities = Activities()
        thisActivities.updateModelWithJSON(json:json)
        self.activiteis = thisActivities
    }
    
    static func getCurrentUser() -> UserModel? {
        let realm = try! Realm()
        if realm.objects(UserModel.self).count > 0 {
            return realm.objects(UserModel.self).first
        }
        return nil
    }
    
    static func addModel(json : JSON) {
        let user = UserModel()
        user.updateModelWithJSON(json: json)
        
        let realm = try! Realm()
        if realm.objects(UserModel.self).filter("email == '\(user.email)'").count > 0 {
            let model = realm.objects(UserModel.self).filter("email == '\(user.email)'").first
            model?.updateModelWithJSON(json: json)
        }
        else {
            realm.add(user)
        }
    }
    //MARK: - APIs
    static func loginAPI(params: [String : Any] , completion: @escaping (_ status: Bool , _ token : String) -> Void){
        var token = ""
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        var apiStatus  : Bool = false
        API.sharedInstance.executeAPI(type: .login, method: .post, params: params) { (status, response, message) in
            SVProgressHUD.dismiss()
            if status == .success{
                apiStatus = true
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                    
                    if let json = getJson() {
                        UserModel.addModel(json: json["user"])
                        token = response["result"]["token"].stringValue
                    }
                    
                }
                DispatchQueue.main.async {
                    completion(apiStatus , token)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    static func getJson()->(JSON?){
        var jsonData :JSON? = JSON()
        if let path = Bundle.main.path(forResource: "generated", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                jsonData = try JSON(data: data)
                print("jsonData:\(jsonData)")
                return jsonData
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            return jsonData
            print("Invalid filename/path.")
        }
        return jsonData
    }
    
    static func sighnUpAPI(params: [String : Any] , completion: @escaping (_ status: Bool , _ token : String) -> Void){
        SVProgressHUD.show()
        var apiStatus  : Bool = false
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .signup, method: .post, params: params) { (status, response, message) in
            var token = ""
            SVProgressHUD.dismiss()
            if status == .success{
                apiStatus = true
                SVProgressHUD.showSuccess(withStatus: "Sign up Successfully")
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                    
                    //                    UserModel.addModel(json: response["result"]["user"])
                    //                    token = response["result"]["token"].stringValue
                }
                DispatchQueue.main.async {
                    completion(apiStatus , token)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    
    static func sendEmailAPI(params: [String : Any] , completion: @escaping (_ verifyCode : String) -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .verifyCode, method: .post, params: params) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            var code : String = ""
            if status == .success{
                SVProgressHUD.showSuccess(withStatus: message)
                code = response["code"].stringValue
                DispatchQueue.main.async {
                    completion(response["code"].stringValue)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    static func resetPasswordAPI(params: [String : Any] , completion: @escaping () -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .updatePassword, method: .post, params: params) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            if status == .success{
                SVProgressHUD.showSuccess(withStatus: message)
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                    // UserModel.addModel(json: response["user"])
                }
                DispatchQueue.main.async {
                    completion()
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    static func updateProfileAPI(params: [String : Any] , completion: @escaping (_ status: Bool ) -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        
        API.sharedInstance.executeAPI(type: .updateProfile, method: .post, params: params) { (status, response, message) in
            var apiStatus : Bool = false
            SVProgressHUD.dismiss()
            if status == .success{
                SVProgressHUD.showSuccess(withStatus: message)
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                    apiStatus = true
                    //UserModel.addModel(json: response["user"])
                }
                DispatchQueue.main.async {
                    completion(apiStatus)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
}
