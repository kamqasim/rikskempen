//
//  Activities.swift
//  RiksKampen
//
//  Created by Ikotel on 12/22/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//


import Foundation
import UIKit
import RealmSwift
import SwiftyJSON

class Activities : Object{
    
    var weekList = List<WeeklyActivities>()
 
    func updateModelWithJSON(json:JSON){
        
        
        let weeklyActivities = (json["weeklyActivities"].arrayValue)
        
        weeklyActivities.forEach { (thisWeek) in
            let thisActivity = WeeklyActivities()
            print(thisWeek)
            thisActivity.updateModelWithJSON(json: thisWeek)
            self.weekList.append(thisActivity)
        }
    }
    
    static func getAllActivities() -> Activities? {
        let realm = try! Realm()
        if realm.objects(Activities.self).count > 0 {
            return realm.objects(Activities.self).first
        }
        return nil
    }
}

class WeeklyActivities : Object{
    
   @objc dynamic var weekName  : String = ""
   @objc dynamic var imagePath : String = ""
   @objc dynamic var weekID    : String = ""
    var daysList = List<ActivityDay>()
    
    func updateModelWithJSON(json:JSON){
        
        self.weekName    = (json["weekName"].stringValue)
        self.weekID    = (json["weekID"].stringValue)
        self.imagePath  = (json["imagePath"].stringValue)
        let days = (json["days"].arrayValue)
        
        days.forEach { (thisDay) in
            let day = ActivityDay()
            print(thisDay)
            day.updateModelWithJSON(json: thisDay)
            self.daysList.append(day)
        }
    }
}

class ActivityDay : Object{
    
   
    var dayActivityList = List<DayActivityList>()
    
    //    "steps": "78648",
    //    "calories": "76482",
    //    "position": "#19"
    
    func updateModelWithJSON(json:JSON){
        
        
        let dayList = (json["dayactivitesList"].arrayValue)
        dayList.forEach { (thisDayactivity) in
            let dayActivity = DayActivityList()
            print(dayActivity)
            dayActivity.updateModelWithJSON(json: thisDayactivity)
            self.dayActivityList.append(dayActivity)
        }
    }
}

class DayActivityList : Object{
    
    @objc dynamic var dayName : String = ""
    @objc dynamic var imagePath : String = ""
    @objc dynamic var dayID : String = ""
    
    @objc dynamic var steps        : String = ""
    @objc dynamic var calories      : String = ""
    @objc dynamic var position      : String = ""
    
    func updateModelWithJSON(json:JSON){
        
        self.dayName    = (json["dayName"].stringValue)
        self.dayID      = (json["dayID"].stringValue)
        self.imagePath  = (json["imagePath"].stringValue)
        
        self.steps = (json["steps"].stringValue)
        self.calories = (json["calories"].stringValue)
        self.position = (json["position"].stringValue)
        print(steps)
    }
}
