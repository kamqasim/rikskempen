//
//  Trainings.swift
//  RiksKampen
//
//  Created by Ikotel on 12/22/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import Foundation
import UIKit
import RealmSwift
import SwiftyJSON

class Trainings : Object{
    
    var weekList = List<WeeklyTrainings>()
    @objc dynamic var weekName : String = ""
    @objc dynamic var imagePath : String = ""
    @objc dynamic var weekID : String = ""
    func updateModelWithJSON(json:JSON){
        
        let weeklyActivities = (json["weeklyTrainings"].arrayValue)
        
        weeklyActivities.forEach { (thisWeek) in
            let thisActivity = WeeklyTrainings()
            print(thisWeek)
            thisActivity.updateModelWithJSON(json: thisWeek)
            self.weekList.append(thisActivity)
        }
    }
    
    static func getAllActivities() -> Trainings? {
        let realm = try! Realm()
        if realm.objects(Trainings.self).count > 0 {
            return realm.objects(Trainings.self).first
        }
        return nil
    }
}

class WeeklyTrainings : Object{
    
    @objc dynamic var weekName : String = ""
    @objc dynamic var imagePath : String = ""
    @objc dynamic var weekID : String = ""
    var daysList = List<TrainingsDay>()
    
    
    func updateModelWithJSON(json:JSON){
        
        self.weekName    = (json["weekName"].stringValue)
        self.weekID    = (json["weekID"].stringValue)
        self.imagePath  = (json["imagePath"].stringValue)
        
        let days = (json["days"].arrayValue)
        
        days.forEach { (thisDay) in
            let day = TrainingsDay()
            print(thisDay)
            day.updateModelWithJSON(json: thisDay)
            self.daysList.append(day)
        }
    }
}

class TrainingsDay : Object{
    
    
    
    var dayTrainingList = List<DayTrainingsList>()
    
    var videoList = List<Video>()
    //    "steps": "78648",
    //    "calories": "76482",
    //    "position": "#19"
    
    func updateModelWithJSON(json:JSON){
        
        
        let dayList = (json["dayTrainingList"].arrayValue)
        dayList.forEach { (thisDayactivity) in
            let dayActivity = DayTrainingsList()
            print(dayActivity)
            dayActivity.updateModelWithJSON(json: thisDayactivity)
            self.dayTrainingList.append(dayActivity)
        }
    }
}

class DayTrainingsList : Object{
    
    @objc dynamic var dayName       : String = ""
    @objc dynamic var imagePath     : String = ""
    @objc dynamic var dayID         : String = ""
    @objc dynamic var steps         : String = ""
    @objc dynamic var calories      : String = ""
    @objc dynamic var position      : String = ""
    
    func updateModelWithJSON(json:JSON){
        
        self.dayName    = (json["dayName"].stringValue)
        self.dayID    = (json["dayID"].stringValue)
        self.imagePath  = (json["imagePath"].stringValue)
        self.steps = (json["steps"].stringValue)
        self.calories = (json["calories"].stringValue)
        self.position = (json["position"].stringValue)
        print(steps)
    }
}

class Video : Object{
    override class func primaryKey() -> String? {
        return "videoId"
    }
    
    @objc dynamic var videoId                   : String = ""
    @objc dynamic var videoName                 : String = ""
    @objc dynamic var videotype                 : String = ""
    @objc dynamic var videoTime                 : String = ""
    
    
    func updateModelWithJSON(json:JSON){
        
        self.videoId                    = (json["userid"].stringValue)
        self.videoName                  = (json["orderid"].stringValue)
        self.videotype                  = (json["itemid"].stringValue)
        self.videoTime                  = (json["itemrname"].stringValue)
    }
    
    static func getCurrentUser() -> Item? {
        let realm = try! Realm()
        if realm.objects(Item.self).count > 0 {
            return realm.objects(Item.self).first
        }
        return nil
    }
    
    //    static func addModel(json : JSON) {
    //        let training = Trainings()
    //        training.updateModelWithJSON(json: json)
    //
    //        let realm = try! Realm()
    //        if realm.objects(Item.self).filter("trainingId == '\(training.trainingId)'").count > 0 {
    //            let model = realm.objects(Trainings.self).filter("trainingId == '\(training.trainingId)'").first
    //            model?.updateModelWithJSON(json: json)
    //        }
    //        else {
    //            realm.add(training)
    //        }
    //    }
}
