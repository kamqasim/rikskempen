//
//  Role.swift
//  RiksKampen
//
//  Created by Ikotel on 12/22/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import SVProgressHUD

class Role : Object{
    
    @objc dynamic var roleId : String  = ""
    @objc dynamic var roleName : String  = ""
    @objc dynamic var height : String  = ""
    @objc dynamic var weight : String  = ""
    @objc dynamic var ptReview : String  = ""
    @objc dynamic var ptBio : String  = ""
    
    //    "roleId": "1",
    //    "roleName": "Contestant",
    //    "height":"32",
    //    "weight": "32"
    //    "ptReview" : "4.3"
    //    "ptBio":""
    
    func updateModelWithJSON(json:JSON){
        
        self.roleId       = (json["roleId"].stringValue)
        self.roleName     = (json["roleName"].stringValue)
        self.height       = (json["height"].stringValue)
        self.weight       = (json["weight"].stringValue)
        self.ptReview     = (json["ptReview"].stringValue)
        self.ptBio        = (json["ptBio"].stringValue)
    }
    
    static func getCurrentUser() -> Role? {
        let realm = try! Realm()
        if realm.objects(Role.self).count > 0 {
            return realm.objects(Role.self).first
        }
        return nil
    }
    
}
