//
//  OrderHistory.swift
//  RiksKampen
//
//  Created by Ikotel on 12/19/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import SVProgressHUD

class Item : Object {
    
    override class func primaryKey() -> String? {
        return "itemId"
    }
    
    @objc dynamic var userId            : String = ""
    @objc dynamic var orderId           : String = ""
    @objc dynamic var itemId            : String = ""
    @objc dynamic var itemName          : String = ""
    @objc dynamic var itemAddress       : String = ""
    @objc dynamic var itemTime          : String = ""
    @objc dynamic var itemStatus        : String = ""
    
    
    func updateModelWithJSON(json:JSON){
        
        self.userId             = (json["userid"].stringValue)
        self.orderId            = (json["orderid"].stringValue)
        self.itemId             = (json["itemid"].stringValue)
        self.itemName           = (json["itemrname"].stringValue)
        self.itemAddress        = (json["itemaddress"].stringValue)
        self.itemTime           = (json["itemtime"].stringValue)
        self.itemStatus         = (json["itemstatus"].stringValue)
    }
    
    static func getCurrentUser() -> Item? {
        let realm = try! Realm()
        if realm.objects(Item.self).count > 0 {
            return realm.objects(Item.self).first
        }
        return nil
    }
    
    static func addModel(json : JSON) {
        let item = Item()
        item.updateModelWithJSON(json: json)
        
        let realm = try! Realm()
        if realm.objects(Item.self).filter("itemId == '\(item.itemId)'").count > 0 {
            let model = realm.objects(Item.self).filter("itemId == '\(item.itemId)'").first
            model?.updateModelWithJSON(json: json)
        }
        else {
            realm.add(item)
        }
    }
}

class OrderHistory: Object {
    
    override class func primaryKey() -> String? {
        return "orderId"
    }
    
    @objc dynamic var userId             : String = ""
    @objc dynamic var orderId            : String = ""
    @objc dynamic var orderName          : String = ""
    @objc dynamic var orderAddress       : String = ""
    @objc dynamic var orderTime          : String = ""
    @objc dynamic var orderStatus        : String = ""
    let orderItemList    = List<Item>()
    
    func updateModelWithJSON(json:JSON){
        
        self.userId     = (json["userid"].stringValue)
        self.orderId   = (json["orderid"].stringValue)
        self.orderName   = (json["ordername"].stringValue)
        self.orderAddress      = (json["orderaddress"].stringValue)
        self.orderTime        = (json["ordertime"].stringValue)
        self.orderStatus        = (json["orderstatus"].stringValue)
    }
    
    static func getCurrentUser() -> OrderHistory? {
        let realm = try! Realm()
        if realm.objects(OrderHistory.self).count > 0 {
            return realm.objects(OrderHistory.self).first
        }
        return nil
    }
    
    static func addModel(json : JSON) {
        let order = OrderHistory()
        order.updateModelWithJSON(json: json)
        
        let realm = try! Realm()
        if realm.objects(OrderHistory.self).filter("orderId == '\(order.orderId)'").count > 0 {
            let model = realm.objects(OrderHistory.self).filter("orderId == '\(order.orderId)'").first
            model?.updateModelWithJSON(json: json)
        }
        else {
            realm.add(order)
        }
    }
}


