//
//  Nutriations.swift
//  RiksKampen
//
//  Created by Ikotel on 12/22/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import Foundation
import UIKit
import RealmSwift
import SwiftyJSON

class Nutriations : Object{
    
    var weekList = List<WeeklyNutriations>()
    
    func updateModelWithJSON(json:JSON){
        
        let weeklyActivities = (json["weeklyNutriations"].arrayValue)
        
        weeklyActivities.forEach { (thisWeek) in
            let thisActivity = WeeklyNutriations()
            print(thisWeek)
            thisActivity.updateModelWithJSON(json: thisWeek)
            self.weekList.append(thisActivity)
        }
    }
    
    static func getAllActivities() -> Nutriations? {
        let realm = try! Realm()
        if realm.objects(Nutriations.self).count > 0 {
            return realm.objects(Nutriations.self).first
        }
        return nil
    }
}

class WeeklyNutriations : Object{
    
    @objc dynamic var weekName : String = ""
    @objc dynamic var imagePath : String = ""
    @objc dynamic var weekID : String = ""
    var daysList = List<NutriationDay>()
    
    func updateModelWithJSON(json:JSON){
        
        self.weekName    = (json["weekName"].stringValue)
        self.weekID    = (json["weekID"].stringValue)
        self.imagePath  = (json["imagePath"].stringValue)
        let days = (json["days"].arrayValue)
        
        days.forEach { (thisDay) in
            let day = NutriationDay()
            print(thisDay)
            day.updateModelWithJSON(json: thisDay)
            self.daysList.append(day)
        }
    }
}

class NutriationDay : Object{
    
 
    var dayNutriationList = List<DayActivityList>()
    
    //    "steps": "78648",
    //    "calories": "76482",
    //    "position": "#19"
    
    func updateModelWithJSON(json:JSON){
        
        
        let dayList = (json["daynutrationList"].arrayValue)
        dayList.forEach { (thisDayactivity) in
            let dayActivity = DayActivityList()
            print(dayActivity)
            dayActivity.updateModelWithJSON(json: thisDayactivity)
            self.dayNutriationList.append(dayActivity)
        }
    }
}

class DayNutriationsList : Object{
    
  
    @objc dynamic var dayName : String = ""
    @objc dynamic var imagePath : String = ""
    @objc dynamic var dayID : String = ""
    @objc dynamic var steps        : String = ""
    @objc dynamic var calories      : String = ""
    @objc dynamic var position      : String = ""
    
    func updateModelWithJSON(json:JSON){
        
        self.steps = (json["steps"].stringValue)
        self.calories = (json["calories"].stringValue)
        self.position = (json["position"].stringValue)
        self.dayName    = (json["dayName"].stringValue)
        self.dayID    = (json["dayID"].stringValue)
        self.imagePath  = (json["imagePath"].stringValue)
        
        print(steps)
    }
}
