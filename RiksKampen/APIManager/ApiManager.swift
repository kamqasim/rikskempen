//
//  ApiManager.swift
//  RiksKampen
//
//  Created by Ikotel on 12/4/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//


import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

typealias completionBlock = (StatusCodes,JSON,String) -> ()

let baseURL = "http://rikskampen.dubaisoftwaresolutions.com/public/index.php/api/"//"http://yourprice.ae/api/"//
//"https://madeeasy.com.pk/"
let stripePublishableKey = "pk_test_7sYu6JZes61iVSyhkmpj6PY8"

enum StatusCodes {
    case success
    case failure
    case authError
}

enum EndPoint : String {
    
    //Account
    
    case login                      = "login"//"login"
    case gethome                    = "get_home.php"
    case signup                     = "userSingUp"//"userSingUp"
    case getItemByCat               = "get_item_by_cat.php"
    case addOrder                   = "add_order.php"
    case verifyCode                 = "verify_code.php"
    case updatePassword             = "update_password.php"
    case updateProfile              = "update_profile.php"
    //Program
    
    //Event
    
    //Exercise
    
    //RSVP
}

class API : NSObject {
    
    static let sharedInstance = API()
    private override init(){
    }
    
    func executeAPI(type: EndPoint,method: HTTPMethod, params: Parameters? = nil, completion: completionBlock?){
        
        URLCache.shared.removeAllCachedResponses()
        
        var request: DataRequest!
        let realm = try! Realm()
        
        var headers = HTTPHeaders()
        
        
        if let user = realm.objects(UserModel.self).first{
            headers = [
                "userName": "\(user.firstName)",
                "userid": "\(user.userid)"
            ]
        }
        
        if let param = params{
            request =  Alamofire.request(baseURL + "\(type.rawValue)", method: method, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    print(response.request as Any)  // original URL request
                    print(response.response as Any) // URL response
                    print(response.result.value as Any)   // result of response serialization
            }
        }else{
            request =   Alamofire.request(baseURL + "\(type.rawValue)", method: method, parameters: params, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    //                    print(response.request as Any)  // original URL request
                    //                    print(response.response as Any) // URL response
                    //                    print(response.result.value as Any)
                    //                    // result of response serialization
            }
        }
        
        request.responseJSON { response in
            
            if (completion != nil){
                if response.response?.statusCode == 401{
                    completion?(.authError,JSON.null,"UnAuthorized User")
//                    let realm = try! Realm()
//                    try! realm.write {
//                        realm.deleteAll()
//                    }
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
                    appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    return
                }
                
                if let error = response.result.error{
                    completion?(.failure,JSON.null,error.localizedDescription)
                    return
                }
                
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    if json["code"].stringValue == "200"{
                        completion?(.success,JSON(value),json["message"].stringValue)
                    }
                    else{
                        
                        if json["status"].stringValue == "error"{
                            completion?(.success,JSON(value),json["message"].stringValue)
                        }else{
                            completion?(.failure,JSON.null,json["message"].stringValue)
                        }
                    }
                    return
                }
                
                completion?(.authError,JSON.null,"Something went wrong, please try again")
            }
        }
    }
}
