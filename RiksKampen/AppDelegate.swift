//
//  AppDelegate.swift
//  RiksKampen
//
//  Created by Ikotel on 12/3/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FacebookCore
import FacebookLogin
import Google
import GoogleSignIn
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate  , GIDSignInDelegate  {

    var window: UIWindow?
    var googleAPIKey = "AIzaSyAy11QPMQf14buf7SlIvjRuWWMiRHQSVb0"
    //"AIzaSyCDK6qOzMVxHSygmaT3kFBtPtdBeJXpZD0"
    var configureError: NSError?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        if let navImage = UIImage(named: "navigationbackGround"){
             UINavigationBar.appearance().setBackgroundImage(navImage, for: .default)
            UINavigationBar.appearance().barTintColor = .white
        }
        
        var navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor(hexString: "FFFFFF")
        
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        GMSServices.provideAPIKey(googleAPIKey)
        GIDSignIn.sharedInstance().clientID = "711619320584-sd63i9lkmbudb4m8mf3nckpqtdd75e7c.apps.googleusercontent.com"
        GGLContext.sharedInstance().configureWithError(&configureError)
        if let configureError = configureError{
            print(configureError.localizedDescription)
        }
        GIDSignIn.sharedInstance().delegate = self
        return SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url as URL,
                                                                sourceApplication: sourceApplication,
                                                                annotation: annotation)
        
        let facebookDidHandle = SDKApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    
        return googleDidHandle || facebookDidHandle
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        AppEventsLogger.activate(application)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

