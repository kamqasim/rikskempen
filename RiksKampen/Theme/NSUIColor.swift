//
//  NSUIColor.swift
//  RiksKampen
//
//  Created by Ikotel on 1/2/19.
//  Copyright © 2019 KamsQue. All rights reserved.
//

import UIKit
import Charts

extension  NSUIColor{
    
    convenience init(red:Int ,green:Int, blue : Int){
        assert(red >= 0, "invalid red Component")
        assert(red >= 0, "invalid green Component")
        assert(red >= 0, "invalid blue Component")
        
        self.init(red: Int(CGFloat(red)/255.0),green: Int(CGFloat(green)/255.0),blue: Int(CGFloat(blue)/255.0))
    }
    
    convenience init(hex: Int){
        self.init(red: (hex >> 16) & 0xFF,
                  green: (hex >> 8) & 0xFF,
                  blue: hex & 0xFF)
    }
}
