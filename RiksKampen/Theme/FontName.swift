//
//  Font.swift
//  RiksKampen
//
//  Created by Ikotel on 12/4/18.
//  Copyright © 2018 KamsQue. All rights reserved.
//

import Foundation
import UIKit

class FontName{
    
    static var segueUI : String = "SegoeUI"
    static var robotoRegular : String = "Roboto-Regular"
    static var robotoMedium : String = "Roboto-Medium"
    static var robotoBold : String = "Roboto-Bold"
    
    static var smallFontSize : CGFloat  = 13.0
    static var normalFontSize : CGFloat  = 15.0
    static var largeFontSize : CGFloat  = 20.0
    
}
